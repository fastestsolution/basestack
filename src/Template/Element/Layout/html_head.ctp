<?= $this->Html->charset() ?>
<?php 
$this->Html->setTemplates([
	'link' => '<a href="'.$url_lang_prefix.'{{url}}"{{attrs}}>{{content}}</a>'
]);
?>

<meta name='keywords' content='<?php echo $this->Fastest->seoText((isset($seo)?$seo:''),$setting,'keywords'); ?>'/>
<meta name='description' content='<?php echo $this->Fastest->seoText((isset($seo)?$seo:''),$setting,'description'); ?>'/>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="robots" content="all,index,follow" />
<meta name='author' content='Fastest Solution, 2019 All Rights Reserved url: http://www.fastest.cz' />
<meta name='dcterms.rights' content='Fastest Solution, 2019 All Rights Reserved url: http://www.fastest.cz' />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no " /> 
<link rel="icon" href="/images/favicon.png" /> 

<meta property="og:title" content="<?php echo $this->Fastest->seoText((isset($seo)?$seo:''),$setting,'title'); ?>" />
<meta property="og:description" content="<?php echo $this->Fastest->seoText((isset($seo)?$seo:''),$setting,'description'); ?>" />
<meta property="og:image" content="<?php echo $this->Fastest->seoText((isset($seo)?$seo:''),$setting,'img'); ?>" />
