<nav id="navigation" class="">
	<a class="menu__toggler menu__toggler--sticked d-md-none" id="main-menu-toggler" href="#" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
		<span class="toggler__icon"></span>
	</a>
	  <?php
		$options = [
			'class'=>'menu menu--horizontal menu--sticked d-md-block none',
			'prefix' => (($articlePrefix != '/') ? $articlePrefix : ''),
			'submenu_class'=>'dropdown',
			'href_class'=>'dropdown-toggle',
			'path'=>$menuItemsPath,
			'id'=> 'main-menu'
		];
		
		echo $this->Fastest->generateMenu($menuItems,$options);
		?>
</nav>  