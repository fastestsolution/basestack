<?php
echo '<div class="col col-6">';
// vychozi formular
if ($formId == 'default'){
    echo $this->Form->create($contactForms,['url'=>'/saveContactForm/', 'class'=>'SaveForm', 'method'=>'POST']);
        echo '<div class="row">';
            echo '<div class="col col-12">';
                echo $this->Form->control('jmeno.value', ['label'=>false,'placeholder' => __('Vaše jméno'),'class'=>'form-control']);
            echo '</div>';
            echo '<div class="col col-12">';
                echo $this->Form->control('jmeno.name', ['type'=>'hidden','value'=>'Jméno']);
            echo '</div>';
            echo '<div class="col col-12">';
                echo $this->Form->control('email.value', ['label'=>false,'placeholder' => __('Email na který Vám můžete odpovědět'),'class'=>'form-control']);
            echo '</div>';
            echo '<div class="col col-12">';
                echo $this->Form->control('email.name', ['type'=>'hidden','value'=>'Email']);
            echo '</div>';
            echo '<div class="col col-12">';
                echo $this->Form->control('telefon.value', ['label'=>false,'placeholder' => __('Telefon na který Vám můžete zavolat'),'class'=>'form-control']);
            echo '</div>';
            echo '<div class="col col-12">';
                echo $this->Form->control('telefon.name', ['type'=>'hidden','value'=>'Telefon']);
            echo '</div>';
            echo '<div class="col col-12">';
                echo $this->Form->control('text.value', ['label'=>false,'placeholder' => __('Vaše zpráva'),'class'=>'form-control','type'=>'textarea']);
            echo '</div>';
            echo '<div class="col col-12">';
                echo $this->Form->control('text.name', ['type'=>'hidden','value'=>'Zpráva']);
            echo '</div>';
            echo '<div class="col col-12">';
                echo $this->Form->button(__('Odeslat'), ['class' => 'btn btn-primary SaveForma']);
            echo '</div>';	
        echo '</div>';	
    echo $this->Form->end();
}
echo '</div>';
?>