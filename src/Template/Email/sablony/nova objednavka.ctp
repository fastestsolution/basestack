<h2>Děkujeme za Vaši objednávku číslo {id}</h2>
<p>Děkujeme Vám za objednávku, zasíláme rekapitulaci objednávky:</p>

<h2>Doručovací adresa</h2>
<table>
	<tr><th>Jméno: </th><td>{user.first_name}</td><th>Příjmení:</th><td>{user.last_name}</td></tr>
	<tr><th>Společnost: </th><td>{user.company_name}</td><th></th><td></td></tr>
	<tr><th>IČO: </th><td>{user.ico}</td><th>DIČ: </th><td>{user.dic}</td></tr>
	<tr><th>Ulice: </th><td>{address1.street} {address1.cp}</td><th>Město:</th><td>{address1.city}</td></tr>
	<tr><th>PSČ: </th><td>{address1.zip}</td><th></th><td></td></tr>
</table>

<h2>Fakturační adresa</h2>
<table>
	<tr><th>Jméno: </th><td>{user.first_name}</td><th>Příjmení:</th><td>{user.last_name}</td></tr>
	<tr><th>Ulice: </th><td>{address2.street} {address2.cp}</td><th>Město:</th><td>{address2.city}</td></tr>
	<tr><th>PSČ: </th><td>{address2.zip}</td><th></th><td></td></tr>
</table>

<h2>Rekapitulace objenávky</h2>
<table>
	<tr>
		<th>Název</th>
		<th>ks</th>
		<th class="text_right">Cena</th>
		<th class="text_right">Cena celkem</th>
	</tr>
		
	{LOOP:shop_order_items}
	<tr>
		<td>{shop_order_items.name}</td>
		<td>{shop_order_items.count}</td>
		<td class="text_right">{shop_order_items.price_vat_per_item}</td>
		<td class="text_right">{shop_order_items.price_vat}</td>
	</tr>
	{ENDLOOP:shop_order_items}
	
	<tr>
		<th>Doprava</th>
		<th colspan="3"></th>
	</tr>
	<tr>
		<td>{shop_transport.name}</td>
		<td></td>
		<td class="text_right">{price_transport}</td>
		<td class="text_right">{price_vat_transport}</td>
	</tr>
	<tr>
		<th>Platba</th>
		<th colspan="3"></th>
	</tr>
	<tr>
		<td>{shop_payment.name}</td>
		<td></td>
		<td class="text_right">{price_payment}</td>
		<td class="text_right">{price_vat_payment}</td>
	</tr>
	<tr>
		<th colspan="3"><h3>Celkem:</h3></th>
		<td class="text_right"><h3>{price_vat}</h3></td>
	</tr>
</table>


{podpis}