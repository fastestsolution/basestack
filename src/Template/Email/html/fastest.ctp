<style>
body {
    font-size:17px;
    color: #23262b;
}
#obal {
    background:#ffffff;
    width:86%;
    margin:auto;
    display:block;
    position:relative;
    padding:10px;
}
#header{
    border-bottom: 1px solid #ffb72b;
    padding-bottom: 20px;
    margin-bottom: 30px;
}
#footer{
    border-top: 1px solid #ffb72b;
    padding-top: 30px; 
    margin-top: 30px;
    font-size: 14px;
}
.col-3{
    width: 100%;
    margin: 0;
    padding: 0;
}

.text_right {
	text-align:right;
}

#subscribe{
    margin-top: -10px;
}
table{
    width:100%;
    padding: 0 8px;
    border-spacing: 0px;
    border-collapse: collapse;
}
table tr th {font-size:15px; text-align:left; padding: 4px 0; background: #ffb72b; }
table tfoot tr td { background: #ffb72b; }
table tr td {font-size:14px;text-align:left; padding: 3px 0;}
.clear{
    clear: both;
}


</style>
<div id="obal" style="background:#ffffff;
    width:86%;
    margin:auto;
    display:block;
    position:relative;
    padding:10px;">
    <div id="header" style="border-bottom: 1px solid #ffb72b;
    padding-bottom: 20px;
    margin-bottom: 30px;">
        <img src="http://varimeprovas.cz/uploaded/logo.png" alt="Vaříme pro vás" id="logo" />
    </div>
    <?= $data ?>
    <div id="footer" style="border-top: 1px solid #ffb72b;
    padding-top: 30px;
    margin-top: 30px;
    font-size: 14px;">
        <div class="col-3" style="width: 100%;
    margin: 0;
    padding: 0;">
            <br/>
            <img src="http://varimeprovas.cz/uploaded/vase_chute.png" alt="Vaše chutě naše starost" id="subscribe" />
            <br/>

        </div>
       <div class="col-3" style="width: 100%;
    margin: 0;
    padding: 0;">
            <strong>LAPET gastro s.r.o</strong><br/>
            Opavská 278/27<br/>
            74301 Bílovec
        </div>
        <div class="col-3" style="width: 100%;
    margin: 0;
    padding: 0;">
            <strong>E-mail: </strong>info@varimeprovas.cz<br/>
            <strong>Tel: </strong> +420 777 010 755<br/>
            <strong>Web: </strong> www.varimeprovas.cz
        </div>
        <div class="clear" style="clear: both;"></div>
    </div>
</div>

