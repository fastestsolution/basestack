<?php  
echo __('<p>Je nám líto, ale zadaná stránka nemůže být zobrazena.</p>
		<h2>Možné příčiny Vašeho problému</h2>
			<ul>
				<li>Chybně zadaná adresa stránky</li>
				<li>Zadaná stránka neexistuje</li>
				<li>Stránka je dočasně nedostupná, zkuste se vrátit později</li>
				<li>Nemáte oprávnění k prohlížení zadané stránky</li>
				<li>Zkuste navštívit <a href="/" title="Zpět na Úvodní stránku">úvodní stránku</a></li>
			</ul>
');
echo '<h2>'.__('Mapa stránek').'</h2>';
$options = [
	'class'=>'sitemap',
	'sitemap'=>true,
	'prefix'=>(isset($modul_shop)?'/text/':''),
	'path'=>$menuItemsPath,
];
//pr($menu_items);
echo $this->Fastest->generateMenu($menuItems,$options);
if (isset($modul_shop)){
	echo '<br />';
	$options = [
		'class'=>'sitemap',
		'sitemap'=>true,
		'path'=>$shop_menu_items_path,
	];
	echo $this->Fastest->generateMenu($shop_menu_items,$options);
}
?>