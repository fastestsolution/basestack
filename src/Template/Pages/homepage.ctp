<section>
    <div class="container">
        <div class="row">
            <h2>Base page for web</h2>
        </div>
        <div class="row">
            <ul>
                <li>Cake 3.7</li>
                <li>Bootstrap 4</li>
                <li>Gulp</li>
                <li>pure JS</li>
                <li>Webpack</li>
            </ul>
            <?php echo $this->cell('ContactForm', ['id' => 'default']); ?>
        </div>
    </div>
</section>

