<!DOCTYPE html>
<html>
<head>
	<?php echo $this->element('Layout/html_head'); ?>
	<?php echo $this->element('monitoring'); ?>
	<?php echo $this->element('Layout/js_css');  ?>
	<?php echo $this->fetch('meta') ?>
	<?php echo $this->fetch('css') ?>
	<?php echo $this->fetch('script') ?>
	<?php
		if (isset($scripts)){if (is_array($scripts)){foreach ($scripts as $link){echo $this->Html->script($link);}} else {echo $this->Html->script($scripts);}}
		if (isset($styles)){if (is_array($styles)){foreach ($styles as $style){echo $this->Html->css($style);} } else {echo $this->Html->css($styles);}} 
	?>
	<?php if (!isset($seo) && isset($title)) $seo = ['title'=>$title]; ?>
	<title><?php echo (isset($breadcrumb)?$this->Fastest->pageTitle($breadcrumb).' | '.$setting['page_title']:$this->Fastest->seoText((isset($seo)?$seo:''),$setting,'title')) ?></title>
</head> 
<body id="body" data-controller="<?php echo $controller; ?>" data-action="<?php echo $action; ?>" data-debug="<?php echo $debug;?>" data-langPref="<?php echo $url_lang_prefix;?>" class="<?php echo (isset($onMainPage) ? 'main-page' : ''); ?>">
  
	<div id="contents">
		<div class="container">
			
			<div id="headerAdopt"></div>

			<div class="row">
					<div class="<?php echo (isset($onMainPage)?'':'wrapper'); ?>">
						<h1 id="h1"><?php echo (isset($title)?$title:'');?></h1>
						<?php echo $this->fetch('content'); ?>
					</div>
				</div>
			</div>
			
			<?php echo $this->element('Layout/footer'); ?>
		</div>
	</div> 

	<?php echo $this->element('Layout/header'); ?>

	<div id="addon"></div>

	<?php echo $this->element('Layout/ga'); ?>
</body>
</html>
