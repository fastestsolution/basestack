<?php
use Cake\Core\Configure;
use Cake\I18n\I18n;
use Cake\I18n\Date;
use Cake\I18n\Time;
use Cake\Cache\Cache;
use Cake\ORM\TableRegistry;

	/**
	 * cakephp Configure:read
	 * @SuppressWarnings(PHPMD)
	 */
	function getConfigure($value){
		return Configure::read($value);
	}
	
	/**
	 * cakephp Configure:write
	 * @SuppressWarnings(PHPMD)
	 */
	function setConfigure($key, $value){
		return Configure::write($key, $value);
	}
	
	/**
	 * cakephp setLocale
	 * @SuppressWarnings(PHPMD)
	 */
	function writeLocale($value){
		return I18n::setLocale($value);
	}
	
	/**
	 * cakephp Cache::write
	 * @SuppressWarnings(PHPMD)
	 */
	function setCache($key, $value){
		return Cache::write($key, $value);
	}
	
	/**
	 * cakephp Cache::read
	 * @SuppressWarnings(PHPMD)
	 */
	function getCache($value){
		return Cache::read($value);
	}
	
	/**
	 * cakephp TableRegistry::get
	 * @SuppressWarnings(PHPMD)
	 */
	function getTableRegistry($value){
		return TableRegistry::get($value);
	}

	/*** ENCODE LONG URL **/	
	function encodeLongUrl($data){
		return strtr(base64_encode(addslashes(gzcompress(json_encode($data),9))), '+/=', '-_,');    
	}

	/*** DECODE LONG URL **/	
	function decodeLongUrl($data){
		$data2 = explode('?',$data);
		$data = $data2[0];
		return json_decode(@gzuncompress(stripslashes(base64_decode(strtr($data, '-_,', '+/=')))),true);
	}
?>