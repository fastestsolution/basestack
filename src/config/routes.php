<?php

use Cake\Core\Plugin;
use Cake\Routing\Router;

use Cake\Core\Configure;

Router::defaultRouteClass('DashedRoute');

Router::scope('/', function ($routes) {


        $routes->connect('/odhlasit-odber/*', ['controller' => 'Pages', 'action' => 'disableNewsletter']);
    //$routes->connect('/ares/*', ['controller' => 'Pages', 'action' => 'display']);
        $routes->connect('/sitemap.xml/*', ['controller' => 'Pages', 'action' => 'googleSitemap']);

        $routes->connect('/clear_cache/*', ['controller' => 'Pages', 'action' => 'clearCache']);
        $routes->connect('/saveContactForm/*', ['controller' => 'Pages', 'action' => 'saveContactForm']);

        // zakomentovat pokud chci pouzivat na HP clanek
        $routes->connect('/', ['controller' => 'Pages', 'action' => 'homepage']);

        $routes->connect(Configure::read('article_prefix').Configure::read('article_detail').'/*', ['controller' => 'Articles', 'action' => 'detail']);
        $routes->connect(Configure::read('article_prefix').'*', ['controller' => 'Articles', 'action' => 'index']);

        $routes->fallbacks('DashedRoute');
    });

    $scopes = function ($routes) {
        $routes->connect(Configure::read('article_prefix').Configure::read('article_detail').'/*', ['controller' => 'Articles', 'action' => 'detail']);
        $routes->connect(Configure::read('article_prefix').'*', ['controller' => 'Articles', 'action' => 'index']);

        $routes->connect('/', ['controller' => 'Pages', 'action' => 'homepage']);
    
};

$languages = Configure::read('languages_list');
foreach ($languages as $lang) {
	Router::scope("/$lang", ['lang' => $lang], $scopes);
}
	
Plugin::routes();
