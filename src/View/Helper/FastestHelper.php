<?php

namespace App\View\Helper;

use Cake\View\Helper;
use Cake\View\View;
use Cake\View\Helper\HtmlHelper;

/**
 * @SuppressWarnings(PHPMD)
 */
class FastestHelper extends Helper
{

  public $helpers = ['Html'];
  public $tableth = "";

	/**
	 * nastaveni page title z breadcrumb
	 */
	function pageTitle($breadcrumb) {
		$out = implode($breadcrumb,' | ');
		return $out;
	}
	
	/**
	 * generovani menu z menu_items
	 */
	function generateMenu($arr, $opt=null) {
		
		//pr($this->request['here']);
		if ($opt != null){
			
			$options = [
				'class'=>'nav',
				'prefix'=>'',
				'submenu_pref'=>'submenu',
				'submenu_class'=>'collapse',
				'href_class'=>'',
				'id'=>'',
			];
			$this->options = array_merge($options,$opt);
		}
		if (!isset($this->options['prefix_default'])){
			$this->options['prefix_default'] = $this->options['prefix'];
		}
		
		$out = '<ul class="'.$this->options['class'].'" id="'.$this->options['id'].'" aria-labelledby="btn-'.$this->options['id'].'">';
		foreach ($arr as $val) {
				if (!empty($val->name)){
				
				//pr($this->options);
				
				$alias = $this->options['prefix_default'].$this->options['path'][$val->id];

				if (isset($this->options['path']) && strpos($this->options['path'][$val->id],'http://') !== false){
					$alias = $this->options['path'][$val->id];	
				}
				$current = ltrim($this->getView()->getRequest()->getAttribute("here"),'/');
				
				
				$active = '';

				if ($alias == $current || ltrim($alias,'/') == $current || rtrim($alias,'/') == $current){
					$active = 'active';
				}
						
				
				if (isset($this->options['open_cat_id']) && $this->options['open_cat_id'] > 0){
					$active = '';
					if (!empty($this->options['open_cat_id']) && $this->options['open_cat_id'] == $val->id){
						$active = 'active';
					}
				}
				
				if (!empty($val->children)) {
						$out .= "<li class=\"menu__item\">";
						
						if (!isset($this->options['sitemap']) && isset($this->options['enable_arrow'])){
							$out .= '<i class="fa fa-caret-down" data-toggle="'.$this->options['submenu_class'].'" data-target="#'.$this->options['submenu_pref'].$val->id.'" aria-expanded="false"></i>';
						}
						$hrefOpt = [
							'title'=>$val->name,
							'data-toggle'=>$this->options['submenu_class'],
							'aria-expanded'=>'false',
							'id'=>'btn-'.$val->id,
							'escape'=>false,
							'class'=>$active.' '.$this->options['href_class'],
							'aria-haspopup'=>"true",
							'aria-expanded'=>"false"
						];	
						if (isset($this->options['data-target']))
						$hrefOpt['data-target'] = '#'.$this->options['submenu_pref'].$val->id;
						//pr($hrefOpt); die();
						$out .= $this->Html->link($val->name.' ',$alias,$hrefOpt);
							if (!isset($this->options['sitemap'])){
								//pr($this->options);
								$this->options = array_merge($this->options,[
									'id'=>$this->options['submenu_pref'].$val->id,
									'class'=>$this->options['class'].' '.$this->options['submenu_class'].'-menu '.$this->options['submenu_class'],
								]);
								
							} 
							if (isset($this->options['sitemap'])){
								$this->options = array_merge($this->options,[
									'class'=>'',
								]);
							}

							$out .= $this->generateMenu($val->children);
						
						$out .= "</li>";
				}
				if (empty($val->children)) {
					$out .= "<li class=\"menu__item\">" . $this->Html->link($val->name,$alias,['title'=>$val->name,'id'=>'m-'.$val->id,'class'=>$active.' '.$this->options['href_class'],]). "</li>";
				}
			}
		}
		$out .= "</ul>";
		return $out;
	}
	
	/**
	 * generovani obrazku
	 */
	function img($imgSet){
		// path noimg
		$noimtPath = '/css/layout/noimg.png';
		
		if (!isset($imgSet['nosize'])){
			$imgSet['nosize'] = false;
		}
		
		$noimg = false;
		if (isset($imgSet['imgs'])){
			$imgs = json_decode($imgSet['imgs'],true);
		}
		
		if (isset($imgSet['first'])){
			if (is_array($imgs))
			foreach($imgs AS $key=>$img){
				if ($key > 0){
					unset($imgs[$key]);
				}
			}
		}
		
		// ostatni fotky pro shop
		if (isset($imgSet['otherImgs'])){
			$otherImgs = array();
			foreach($imgs AS $key=>$img){
				if ($key > 0){
					$otherImgs[] = $img;
					unset($imgs[$key]);
				}
			}
		}
		if (!is_array($imgs)){
			$imgs = [
				0=>['file'=>'#']
			];
		}
		
		foreach($imgs AS $img){
			$imgTitle = ((isset($img['title']) && !empty($img['title']))?$img['title']:(isset($imgSet['title']) && !empty($imgSet['title']))?$imgSet['title']:'Foto');
			
			$imgLink = '/img/';
			
			$imgParams = [];
			//pr($imgSet);
			if (isset($imgSet['path'])){
				$img['file'] = $imgSet['path'].$img['file'];
			}
			//pr($imgSet);
			//pr($img['file']);
			if (strpos($img['file'],'://') !== false){
				$imgParams['file'] = $img['file'];
				if (!$this->urlExists($imgParams['file'])){
					 $noimg = true;
					 $imgParams['file'] = '.'.$noimtPath;
				}
			} 
			if (strpos($img['file'],'://') === false){
				$imgParams['file'] = '.'.$img['file'];
				if (!file_exists($imgParams['file'])){
					 $noimg = true;
					 $imgParams['file'] = '.'.$noimtPath;
				}
			
			}
			//pr($imgParams);pr($noimg);die();
			if (isset($imgSet['class'])) $imgClass = $imgSet['class'];
			if (isset($imgSet['bg'])) $imgParams['bg'] = $imgSet['bg'];
			if (isset($imgSet['width'])) $imgParams['w'] = $imgSet['width'];
			if (isset($imgSet['height'])) $imgParams['h'] = $imgSet['height'];
			
			
			$imgParamsOther['w'] = (isset($imgSet['width_other'])?$imgSet['width_other']:120);
			$imgParamsOther['h'] = (isset($imgSet['height_other'])?$imgSet['height_other']:120);
			
			//pr($imgParams);
			//die();
			
			$imgCode = $imgLink.'?params='.$this->encode_long_url($imgParams);
			//pr($imgCode);
			//die();
			$out = '';
			if (isset($imgSet['otherImgs'])){
				$out .= '<div class="solo_image" id="main_image">';
			}
			if (isset($imgSet['only_url'])){
				$out = $imgCode;
				return $out;
			}
			// zoom foto
			if (isset($imgSet['zoom'])){
				$image = '<div data-original="'.$imgCode.'" class="img lazy '.(isset($imgClass)?$imgClass:'').'"></div> ';
				if ($noimg == false){
					$image = $this->Html->link('<div data-original="'.$imgCode.'" class="zoom_foto img lazy '.(isset($imgClass)?$imgClass:'').'"></div>',$imgParams['file'],array('title'=>$imgTitle,'data-rel'=>'lightbox-atomium','escape'=>false));
				}
			
			// link to url
			} 
			if (isset($imgSet['link'])){
				//pr($imgSet['link']);die();
				$image = $this->Html->link('<div data-original="'.$imgCode.'" class="zoom_foto lazy img '.(isset($imgClass)?$imgClass:'').'"></div>',$imgSet['link'],array('title'=>$imgTitle,'escape'=>false));
			
			// only img
			}
			if (!isset($imgSet['link'])){
				$image = '<div data-original="'.$imgCode.'" class="img lazy '.(isset($imgClass)?$imgClass:'').'"></div> ';
				//$image = $this->Html->image('#',array('data-original'=>$imgCode,'data-width'=>$imgSet['width'],'data-height'=>$imgSet['height'],'data-nosize'=>$imgSet['nosize'],'title'=>$imgTitle,'alt'=>$imgTitle,'class'=>'lightbox-atomium  lazy '.(isset($imgClass)?$imgClass:'')),null,false);
			}
			
			$out .= $image;
			
			if (isset($imgSet['otherImgs'])){
				$out .= '</div>';
			}
			if (isset($otherImgs) && count($otherImgs)>0){
				$out .= '<div class="otherImgs" id="otherImgs">';
					foreach($otherImgs AS $img2){
						$imgLink = '/image_resize.php';
						$imgParams = [];
						//pr($img2);
						if (isset($imgSet['path'])){
							$img2['file'] = $imgSet['path'].$img2['file'];
						}
						
						if (strpos($img2['file'],'://') !== false){
							$img2['file'] = $img2['file'];
							if (!$this->urlExists($img2['file'])){
								 $noimg = true;
								 $img2['file'] = $noimtPath;
							}
						}
						if (strpos($img2['file'],'://') === false){
							$img2['file'] = '.'.$img2['file'];
							if (!file_exists($img2['file'])){
								 $noimg = true;
								 $img2['file'] = $noimtPath;
							}
						
						}
						$imgParams['file'] = $img2['file'];
						
						$imgParams['bg'] = $imgSet['bg'];
						$imgParams['w'] = $imgParamsOther['w'];
						$imgParams['h'] = $imgParamsOther['h'];
						
						//pr($imgParams);die();
						$imgCode = $imgLink.'?params='.$this->encode_long_url($imgParams);
						$out .= $this->Html->link('<div data-original="'.$imgCode.'" class="lazy img zoom_foto '.(isset($imgClass)?$imgClass:'').'"></div>',$imgParams['file'],array('title'=>$imgTitle,'data-rel'=>'lightbox-atomium','class'=>'link','escape'=>false));
						
						
					}
				$out .= '</div>';
			}
		}
		echo $out;
	}
	
	/**
	 * overeni zda je vzdalene url dostupne
	 */
	function urlExists($url) {
			// Version 4.x supported
			$handle = curl_init($url);
			if (false === $handle) {
				return false;
			}
			curl_setopt($handle, CURLOPT_HEADER, false);
			curl_setopt($handle, CURLOPT_FAILONERROR, true);  // this works
			curl_setopt($handle, CURLOPT_HTTPHEADER, Array("User-Agent: Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.15) Gecko/20080623 Firefox/2.0.0.15")); // request as if Firefox   
			curl_setopt($handle, CURLOPT_NOBODY, true);
			curl_setopt($handle, CURLOPT_RETURNTRANSFER, false);
			$connectable = curl_exec($handle);
			curl_close($handle);
			return $connectable;
	}
	
	/**
	 * generovani seo textu
	 */
	function seoText($seoData,$setting,$type){
		$result = '';
		switch($type){
			case 'title': $result = $this->orez((isset($seoData[$type]) && !empty($seoData[$type])?$seoData[$type].' | '.$setting['page_title']:$setting['page_title']),70); break;
			case 'keywords': $result = $this->orez((isset($seoData[$type]) && !empty($seoData[$type])?$seoData[$type]:$setting['keywords']),50); break;
			case 'description': $result = $this->orez((isset($seoData[$type]) && !empty($seoData[$type])?$seoData[$type]:$setting['description']),150); break;
			case 'img': $result = (isset($seoData[$type])?$seoData[$type]:'http://'.$this->request->env('HTTP_HOST').'/css/layout/logo_large.png'); break;
			default: $result = $seoData[$type]; break;
		}
		return $result;
	}
		
	/**
	 * orezani textu
	 */
	function orez($text,$limit){
		$replace = array(
			'&nbsp;'=>' ',
			'&'=>'&amp;',
			"\n"=>' ',
			"\t"=>' ',
			'  '=>' ',
			'   '=>' ',
			'?'=>'? ',
		);
		
		if (mb_strlen($text) <= $limit) {
			$output = strip_tags ($text);
		}
		if (mb_strlen($text) > $limit) {
			mb_internal_encoding("UTF-8");
			$output =  strip_tags($text);
			$output = mb_substr($output, 0, $limit);
		   // $output = str_replace($in,$out,$output);
			$output = strtr($output,$replace);
			$pos = mb_strrpos($output, " ");
		  	$output = trim(mb_substr($output, 0, $pos) . "...");
		}
		$tra = array(
			'"'=>' ',
			'\''=>' ',
			"\n"=>' ',
		); 
		$output = htmlspecialchars("$output", ENT_QUOTES); 
		$output = strtr($output, $tra);
		return $output;
	}
	
	/*** 
	 * ENCODE LONG URL 
	 */
	public function encodeLongUrl($data){
		 return strtr(base64_encode(addslashes(gzcompress(json_encode($data),9))), '+/=', '-_,');    
	}
	
	/**
	 * DECODE LONG URL 
	 */	
	public function decodeLongUrl($data){
		$data2 = explode('?',$data);
		$data = $data2[0];
		return json_decode(gzuncompress(stripslashes(base64_decode(strtr($data, '-_,', '+/=')))),true);
	}
	
	/**
	 * formatovani ceny
	 */
	public function price($price,$menaSuf = array()){
		
		//pr($menaSuf);
		$symbolBefore = '';
		$kurz = null;
		$count = 1;
		$decimal = 0;
		$symbolAfter = ',-';
		extract($menaSuf);
		
		$white = array(" " => "");
		if ($kurz!=null)
			$price = ($price/strtr($kurz,',','.'))*$count;
		if ($price != null)
			return $symbolBefore.number_format(strtr($price,$white), $decimal, '.', ' ').$symbolAfter;
		else
			return $symbolBefore.number_format(strtr(0.00,$white), $decimal, '.', ' ').$symbolAfter;	
	}
	
	/**
	 * prevod na ceske datum
	 */
	function czechDate($date){
		if (!empty($date) && $date!='0000-00-00'){
		$date = strtotime($date);
		// $mesice = array ("ledna", "února", "března", "dubna", "května", "června", "července", "srpna", "září", "října", "listopadu", "prosince");	
		//return date('d',$date).'. '.$mesice[date ("n",$date) - 1].' '.date('Y',$date);
		return date('d',$date).'.'.date ("m",$date).'.'.date('Y',$date);
		}
	}
	
	

}