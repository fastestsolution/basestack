<?php
namespace App\View;

use Cake\Core\Configure;
use Cake\View\View;

class AppView extends View
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading helpers.
     *
     * e.g. `$this->loadHelper('Html');`
     *
     * @return void
     */
    public function initialize()
    {
        $this->loadHelper('WyriHaximus/MinifyHtml.MinifyHtml');
    }
}
 