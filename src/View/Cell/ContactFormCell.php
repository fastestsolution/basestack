<?php
namespace App\View\Cell;

use Cake\View\Cell;

class ContactFormCell extends Cell
{

  public function display($formId = 'default')
  {
    $this->loadModel("ContactForms");
    $contactForms = $this->ContactForms->newEntity();
    $this->set("contactForms", $contactForms);

    $this->set('formId', $formId);
	
  }

}