<?php
namespace App\Model\Behavior;

use Cake\ORM\Behavior;

class ValidatorBehavior extends Behavior
{
	/**
	 * inicializace
	 * 
	 * @SuppressWarnings(PHPMD)
	 */
	public function initialize(array $config){
	}
	/**
	 * check error if form save
	 * @SuppressWarnings(PHPMD)
	 */
	public function checkError($entity=null){
		$result = [];
		if (!isset($entity->spam) || $entity->spam != 123){
			$result = ['result'=>false,'m'=>'Jste spam'];
		}
		
		if ($entity != null && $entity->errors()){
			$message = '';
			$invalidList = array();
			$convertValid = [
				'date_'=>'date-',
				'user_'=>'user-',
			];
			foreach($entity->errors() AS $k=>$mm){
				foreach($mm AS $mk=>$m){
					if (is_array($m)){
						$invalidList[] = $k.'-'.strtr($mk,$convertValid);
						foreach($m AS $kk=>$m2){
							if (is_array($m2)){
								foreach($m2 AS $mk2=>$m3){
									if (is_array($m3)){
										$invalidList[] = $k.'-'.$mk.'-'.$kk.'-'.strtr($mk2,$convertValid);
										
										foreach($m3 AS $mm3){
											$message.= $mm3.'<br />';
										}
									} else {
										$invalidList[] = $k.'-'.$mk.'-'.strtr($kk,$convertValid);
										$message.= $m3.'<br />';
									}
								}
							} else {
								$message .= $m2.'<br />';
							
							}
							
						}
						
					} else {
						$message .= $m.'<br />';
						
					}
					
				}
				$invalidList[] = strtr($k,$convertValid);
			}
			$result = ['result'=>false,'message'=>$message,'invalid'=>$invalidList];

		}
		if (!empty($result)){
			die(json_encode($result));
		}
	}

}
?>