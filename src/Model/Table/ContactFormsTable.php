<?php
namespace App\Model\Table;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;

use Cake\Utility\Hash;
use Cake\I18n\Time;

class ContactFormsTable extends Table
{

  public function initialize(array $config)
  {
	  
    parent::initialize($config);
    $this->addBehavior('Trash');
		$this->addBehavior('Timestamp');
		$this->addBehavior('Validator');
  }
   
	public function validationDefault(Validator $validator)
	{
		$validator
		->requirePresence('jmeno', 'create',  __("Musíte zadat jméno"))
		->notEmpty('jmeno', __("Musíte zadat jméno"))
		
		->requirePresence('email', 'create',  __("Musíte zadat email"))
		->notEmpty('email', __("Musíte zadat email"))
		
		;
		return $validator;
	}
	
}
