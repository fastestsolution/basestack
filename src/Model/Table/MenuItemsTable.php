<?php
namespace App\Model\Table;

use App\Model\Entity\Project;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;

use Cake\Utility\Hash;
use Cake\I18n\Time;

class MenuItemsTable extends Table
{
  public function initialize(array $config)
  {
    parent::initialize($config);
	
	$this->addBehavior('Timestamp');
  }
	
	
	// get full path
	public function getFullPath($type=null,$prefix=null){
		$menuItems = getTableRegistry("MenuItems");
		$this->prefix = $prefix;
		$this->pathList = [];
		$this->pathListGoogle = [];
		
			$query = $menuItems->find('threaded')
				->where([])
				->select([
					'id',
					'name',
					'alias',
					'level',
					'parent_id',
					'lft',
					'rght',
					'spec_url',
				]);
			
			$pathData = $query->toArray();
		
		if ($pathData){
			$this->genPath($pathData);
		}
		//pr($data);
		if ($type == 'google'){
			return $this->pathListGoogle;
		}

		return $this->pathList;
	}	
  
	// gen path recursive
	function genPath($data,$parent=null){
			
		foreach($data AS $d){
			
			
			if (empty($d->spec_url)){
				$url = (($parent != null)?$this->pathList[$parent]:'/').$d->alias.'/';
				$urlGoogle = (($parent != null)?$this->pathListGoogle[$parent]['url']:'/').(($parent == null)?$this->prefix:'').$d->alias.'/';
			}
			
			if (!empty($d->spec_url)){
				$url = $urlGoogle = $d->spec_url;
			}
			
			$this->pathList[$d->id] = $url;
			$this->pathListGoogle[$d->id] = [
				'url'=>$urlGoogle,
				'modified'=>$d->modified,
			];
			if (!empty($d->children)){
				$this->genPath($d->children,$d->id);
			}
		}
	}
  
  

  

}
