<?php
namespace App\Model\Table;

use App\Model\Entity\Project;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;

use Cake\Utility\Hash;
use Cake\I18n\Time;

class ArticlesTable extends Table
{

  public function initialize(array $config)
  {
	  
    parent::initialize($config);
	
	$this->addBehavior('Timestamp');
  }

  
	// generate list from threaded
	public function findCatsId($path,$data){
		$ids = [];
		foreach($path AS $pathData){
			$ids[] = array_search($pathData,$data);
			
		}
		if (empty($ids[0])){
			$ids = '';
		}
		return $ids;
		
	}
  
	// generate list from threaded
	public function menuItems($path,$data){
		$this->menu_items_list = [];
		$this->menu_items_list_name = [];
		
		$this->generateList($data);
		$idsCats = $this->findCatsId($path,$this->menu_items_list);
		$result = '';
	
		if (!empty($idsCats)){
		
		
		rsort($idsCats);
		//pr($idsCats);die();
		$breadcrumb = [];
		foreach($idsCats AS $i){
			$breadcrumb[$i] = $this->menu_items_list_name[$i];
		}
		//pr($this->shop_menu_items_list_name);
		//pr($breadcrumb);die();
		$idsMenu = $this->getConnectionList($idsCats);
		
		
		$result = [
			'menu_items_list_name'=>$this->menu_items_list_name,
			'menu_list'=>$this->menu_items_list,
			'ids'=>$idsMenu,
			'breadcrumb'=>$breadcrumb,
		]; 
		}

		return $result;
	}
	
	
	public function generateList($data){
			foreach($data AS $d){
				$this->menu_items_list[$d->id] = $d->alias;
				$this->menu_items_list_name[$d->id] = $d->name;
				if (isset($d->children)){
					$this->generateList($d->children);
				}
			}	
	}
	
	/**
	 * ziskani product id z connection dle catId
	 * @SuppressWarnings(PHPMD)
	 */
	public function getConnectionList($catId){
		/**
		 * 
		 */
		$articleMenuItems = TableRegistry::get("ArticleMenuItems");
		
		$conditions = ['menu_item_id IN'=>$catId];
		
			$query = $articleMenuItems->find('list',[
				'keyField' => 'article_id',
				'valueField' => 'menu_item_id'
			]);
			$query->where($conditions);
			$dataLoad = $query->toArray();
		
		$data = [];
		
		foreach($dataLoad AS $k=>$dataConnect){
			$data[] = $k;
		}
		//$data = array_flip($data);
		//pr($data);
		return $data;  
	}	
	
	
  

}
