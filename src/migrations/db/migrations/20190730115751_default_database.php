<?php

use Phinx\Migration\AbstractMigration;

class DefaultDatabase extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
	{
		// create the table article_menu_items
		$table = $this->table('article_menu_items');
		$table
			->addColumn('article_id', 'integer')
			->addIndex(['article_id'], [])
			->addColumn('menu_item_id', 'integer')
			->addIndex(['menu_item_id'], [])
			->create();

		// create the table articles
		$table = $this->table('articles');
		$table
			->addColumn('system_id', 'integer')
			->addIndex(['system_id'], [ ])
			->addColumn('name', 'string',['null' => true, 'limit' => 250])
			->addColumn('alias', 'string',['null' => true, 'limit' => 250])
			->addColumn('content', 'text',['null' => true, ])
			->addColumn('images', 'text', ['null' => true, ])
			->addColumn('attachments', 'text',['null' => true, ])
			->addColumn('seo_keywords', 'string',['null' => true, ])
			->addColumn('seo_description', 'string',['null' => true, ])
			->addColumn('additional_html', 'text',['null' => true, ])
			->addColumn('status', 'integer',['null' => true, 'limit' => 1, 'default' => 1])
			->addIndex(['status'], [])
			->addColumn('trash', 'datetime',['null' => true, 'default'=> null ])
			->addIndex(['trash'], [])
			->addColumn('created', 'datetime',['null' => true, 'default'=> null ])
			->addColumn('modified', 'datetime',['null' => true, 'default'=> null ])
			
			->create();

		// create the cms_groups
		$table = $this->table('cms_groups');
		$table
			->addColumn('name', 'string',['null' => true, 'limit' => 250])
			->addColumn('permission', 'text',['null' => true, ])
			->addColumn('nd', 'integer',['null' => true, 'limit' => 1, 'default' => 0])
			->addColumn('status', 'integer',['null' => true, 'limit' => 1, 'default' => 1])
			->addIndex(['status'], [])
			->addColumn('trash', 'datetime',['null' => true, 'default'=> null ])
			->addIndex(['trash'], [])
			->addColumn('created', 'datetime',['null' => true, 'default'=> null ])
			->addColumn('updated', 'datetime',['null' => true, 'default'=> null ])
			
			->create();
		
		// create the cms_users
		$table = $this->table('cms_users');
		$table
			->addColumn('user_name', 'string',['null' => true, 'limit' => 100])
			->addColumn('name', 'string',['null' => true, 'limit' => 100])
			->addColumn('jmeno', 'string',['null' => true, 'limit' => 100])
			->addColumn('prijmeni', 'string',['null' => true, 'limit' => 100])
			->addColumn('ulice', 'string',['null' => true, 'limit' => 100])
			->addColumn('okres', 'string',['null' => true, 'limit' => 100])
			->addColumn('stat_id', 'integer',['null' => true, 'limit' => 11])
			->addColumn('kraj', 'string',['null' => true, 'limit' => 100])
			->addColumn('mesto', 'string',['null' => true, 'limit' => 100])
			->addColumn('telefon', 'string',['null' => true, 'limit' => 100])
			->addColumn('email', 'string',['null' => true, 'limit' => 100])
			->addColumn('mobil', 'string',['null' => true, 'limit' => 100])
			->addColumn('icq', 'string',['null' => true, 'limit' => 100])
			->addColumn('psc', 'string',['null' => true, 'limit' => 100])
			->addColumn('titul_pred', 'string',['null' => true, 'limit' => 100])
			->addColumn('titul_za', 'string',['null' => true, 'limit' => 100])
			->addColumn('prefix', 'string',['null' => true, 'limit' => 100])
			->addColumn('skype', 'string',['null' => true, 'limit' => 100])
			->addColumn('company', 'string',['null' => true, 'limit' => 100])
			->addColumn('funkce', 'string',['null' => true, 'limit' => 100])
			->addColumn('facebook', 'integer',['null' => true, 'limit' => 4])
			->addColumn('heslo', 'string',['null' => true, 'limit' => 100])
			->addColumn('alias_', 'string',['null' => true, 'limit' => 100])
			->addColumn('img', 'text',['null' => true, ])
			->addColumn('cms_group_id', 'integer',['null' => true, 'limit' => 11])
			->addColumn('nd', 'integer',['null' => true, 'limit' => 1, 'default' => 0])
			->addColumn('status', 'integer',['null' => true, 'limit' => 1, 'default' => 1])
			->addIndex(['status'], [])
			->addColumn('trash', 'datetime',['null' => true, 'default'=> null ])
			->addIndex(['trash'], [])
			->addColumn('created', 'datetime',['null' => true, 'default'=> null ])
			->addColumn('updated', 'datetime',['null' => true, 'default'=> null ])
			->addColumn('last_log', 'datetime',['null' => true, 'default'=> null ])

			->create();
		
		// create table contact_forms
		$table = $this->table('contact_forms');
		$table
			->addColumn('email', 'string',['null' => true, 'limit' => 100])
			->addColumn('text', 'text',['null' => true, ])
			->addColumn('status', 'integer',['null' => true, 'limit' => 1, 'default' => 1])
			->addIndex(['status'], [])
			->addColumn('trash', 'datetime',['null' => true, 'default'=> null ])
			->addIndex(['trash'], [])
			->addColumn('created', 'datetime',['null' => true, 'default'=> null ])
			->addColumn('modified', 'datetime',['null' => true, 'default'=> null ])

			->create();
		
		// create table meni_items
		$table = $this->table('menu_items');
		$table
			->addColumn('system_id', 'integer',['null' => true, 'limit' => 11])	
			->addColumn('parent_id', 'integer',['null' => true, 'limit' => 11])
			->addIndex(['parent_id'], [])
			->addColumn('level', 'integer',['null' => true, 'limit' => 11])
			->addColumn('lft', 'integer',['null' => true, 'limit' => 11])
			->addColumn('rght', 'integer',['null' => true, 'limit' => 11])
			->addColumn('name', 'string',['null' => true, 'limit' => 100])
			->addColumn('alias', 'string',['null' => true, 'limit' => 100])
			->addColumn('description', 'string',['null' => true, 'limit' => 100])
			->addColumn('spec_url', 'integer',['null' => true, 'limit' => 10, 'default' => 0])
			->addColumn('show_menu', 'integer',['null' => true, 'limit' => 1, 'default' => 1])
			->addColumn('status', 'integer',['null' => true, 'limit' => 1, 'default' => 1])
			->addIndex(['status'], [])
			->addColumn('trash', 'datetime',['null' => true, 'default'=> null ])
			->addIndex(['trash'], [])
			->addColumn('created', 'datetime',['null' => true, 'default'=> null ])
			->addColumn('modified', 'datetime',['null' => true, 'default'=> null ])

			->create();

		// create table settings
		$table = $this->table('settings');
		$table
			->addColumn('type', 'string',['null' => true, 'limit' => 100])
			->addColumn('content', 'text',['null' => true])
			->addColumn('system_id', 'integer',['null' => true, 'limit' => 11])
			->addColumn('trash', 'datetime',['null' => true, 'default'=> null ])
			->addIndex(['trash'], [])
			->addColumn('created', 'datetime',['null' => true, 'default'=> null ])
			->addColumn('modified', 'datetime',['null' => true, 'default'=> null ])

			->create();
		
		// create table smallboxes
		$table = $this->table('smallboxes');
		$table
			->addColumn('name', 'string',['null' => true, 'limit' => 100])
			->addColumn('content', 'text',['null' => true])
			->addColumn('system_id', 'integer',['null' => true, 'limit' => 11])
			->addColumn('trash', 'datetime',['null' => true, 'default'=> null ])
			->addIndex(['trash'], [])
			->addColumn('created', 'datetime',['null' => true, 'default'=> null ])
			->addColumn('modified', 'datetime',['null' => true, 'default'=> null ])
			->addColumn('nd', 'integer',['null' => true, 'limit' => 1, 'default' => 0])
			->addColumn('status', 'integer',['null' => true, 'limit' => 1, 'default' => 1])
			->addIndex(['status'], [])
			->create();
		
				
	}
}
