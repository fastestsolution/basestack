<?php


use Phinx\Seed\AbstractSeed;

class DefaultData extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $data = [
            [
                'id'    => 1,
                'user_name' => 'admin',
                'name' => 'admin',
                'heslo' => md5('admin'),
                'created' => date('Y-m-d H:i:s'),
                'updated' => date('Y-m-d H:i:s'),
            ]
        ];

        $posts = $this->table('cms_users');
        $posts->insert($data)
            ->save();

        $data = [
            [
                'id'    => 1,
                'name' => 'administrator',
                'permission' => 'a:6:{s:9:"cms_users";a:5:{s:5:"index";s:1:"3";s:4:"edit";s:1:"3";s:5:"trash";s:1:"3";s:6:"status";s:1:"3";s:3:"add";s:1:"3";}s:10:"cms_groups";a:5:{s:5:"index";s:1:"3";s:4:"edit";s:1:"3";s:5:"trash";s:1:"3";s:6:"status";s:1:"3";s:3:"add";s:1:"3";}s:8:"settings";a:5:{s:5:"index";s:1:"3";s:4:"edit";s:1:"3";s:5:"trash";s:1:"3";s:6:"status";s:1:"3";s:3:"add";s:1:"3";}s:10:"menu_items";a:5:{s:5:"index";s:1:"3";s:4:"edit";s:1:"3";s:5:"trash";s:1:"3";s:6:"status";s:1:"3";s:3:"add";s:1:"3";}s:12:"article_news";a:5:{s:5:"index";s:1:"3";s:4:"edit";s:1:"3";s:5:"trash";s:1:"3";s:6:"status";s:1:"3";s:3:"add";s:1:"3";}s:13:"smallbox_news";a:5:{s:5:"index";s:1:"3";s:4:"edit";s:1:"3";s:5:"trash";s:1:"3";s:6:"status";s:1:"3";s:3:"add";s:1:"3";}}',
                'created' => date('Y-m-d H:i:s'),
                'updated' => date('Y-m-d H:i:s'),
            ]
        ];

        $posts = $this->table('cms_groups');
        $posts->insert($data)
            ->save();
    }
}
