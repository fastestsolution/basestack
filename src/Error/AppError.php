<?php
namespace App\Error;
use Rollbar\Rollbar;
use Rollbar\Payload\Level;
use Cake\Error\BaseErrorHandler;
use Cake\Error\ExceptionRenderer;
use Cake\Core\Configure;

class AppError extends BaseErrorHandler
{
    protected $options = [];

    /**
     * zobrazeni warning
     * @SuppressWarnings(PHPMD)
     */
    public function _displayError($error, $debug)
    {
        // echo '<div style="background:red; color:#fff;padding: 3px;">Warning aplikace!</div>';
        // echo '<div style="background:#efefef; color:#000;padding: 3px;">'. var_dump($error) .'</div>';
        // pr($error);
    }

    /**
     * zachyceni exception z cake
     * @SuppressWarnings(PHPMD)
     */
    public function _displayException($exception) 
    {

        $this->initReporting();
        echo '<style>
        .error {
            width: 96%;
            background: #fff;
            font-family: arial;
            position: absolute;
            top: 5px;
            left: 5px;
        }
        .error .title {
            display: block;
            color: #ff0000;
            font-size: 20px;
            margin-bottom: 5px;
            text-align: center;
            width: 100%;
        }
        .error .info {
            width: 100%;
            border: 1px solid #ccc;
            background: #efefef;
            padding: 5px;
        }
        .error .pretty {
            text-align: center;
        }
        .error .exception {
            background: #efefef;
            height: 300px;
            overflow: auto;
        }
        </style>';

        echo '<div class="error">';   
            echo '<div class="title">Chyba aplikace!</div>';
            if (getConfigure('debug') == 1){
                echo '<div class="info">';
                    echo $exception->getMessage().' <br />';
                        echo "FILE: ".$exception->getFile() . "<br>";
                        echo 'LINE: '.$exception->getLine();
                
                echo '</div>';
            }
            echo '<div class="pretty">';
            echo '<p>Nebojte tato chyba byla odeslaná na podporu pro její diagnostiku</p>';
            echo '</div>'; 
            if (getConfigure('debug') == 1){   
                echo '<div class="exception">';
                    pr($exception);
                echo '</div>';
            }
        echo '</div>';
        // ExceptionRenderer::render();
    }
    
    /**
     * kontrola o jake prostredi se jedna
     * @SuppressWarnings(PHPMD)
     */
    private function checkEnv(){
        if (strpos($_SERVER['HTTP_HOST'],'localhost') > 0){
            $env = 'local';
        } else if (strpos($_SERVER['HTTP_HOST'],'fastestdev.cz') > 0){
            $env = 'development';
        } else {
            $env = 'production';
        }
        return $env;
    }

    /**
     * inicializace reportovaci aplikace
     * @SuppressWarnings(PHPMD)
     */
    public function initReporting(){
        
        $env = $this->checkEnv();
        
        session_name('NMCORE');
		if (session_status() < 1) {
            session_start();
        }
        // print_r($_SESSION);die('bb');
        
        if (($env != 'local' || getConfigure('monitoring')['enableLocal']) && !is_null(getConfigure('monitoring')['post_server_item'])) {
            
            $config = array(
                'access_token' => getConfigure('monitoring')['post_server_item'],
                'environment' => $env,
                // 'capture_username' => true,
            );  
            if (isset($_SESSION['Auth']['User']['id'])){
                $config['person'] = array(
                    'id' => $_SESSION['Auth']['User']['id'], // required - value is a string
                    'username' => $_SESSION['Auth']['User']['name'], // optional - value is a string
                );
            }
            Rollbar::init($config);
        }
        $_ENV['env'] = $env;
    }

}