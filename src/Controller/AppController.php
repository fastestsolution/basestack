<?php
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Database\Schema\Table;
use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;
use Cake\Cache\Cache;
use Cake\View\CellTrait;

// jazyky pro preklad
use Cake\I18n\I18n;
use Cake\I18n\Date;
use Cake\I18n\Time;

Date::$defaultLocale = 'cs-CZ';
Date::setToStringFormat('dd.MM.yyyy');

class AppController extends Controller
{
	var $jsClass = [];
	var $paginationLimit = 24;
	var $conditionsTrans = [
			'name',
			'alias',
			'description',
	];
	
	public function beforeFilter(Event $event)
	{
		$this->Session = $this->request->getSession();
		$this->Security->setConfig('unlockedFields', ['spam']);
		$this->Security->setConfig('unlockedActions', [
			//'basket_detail',
		]);
		$this->initReporting();

		// load JS and CSS from MIN JS
		$this->loadJs();

		if ($this->request->getAttribute("here") == '/'){
			$this->set('onMainPage',true);
		}
		$this->loadSettings();
		return parent::beforeFilter($event);
	}
	
	/**
	 * zakladni nacteni
	 */
	public function initialize()
	{
		parent::initialize();
		$this->Session = $this->request->getSession();
			
	
		$this->loadLanguages();
		$this->loadSelectConfig();
		$this->checkIE();
		
		$this->loginComponent();

		$this->loadComponent('Paginator',['limit' => $this->paginationLimit]);
		$this->loadComponent('RequestHandler');
		$this->loadComponent('Flash');
		$this->loadComponent('Csrf');
		$this->loadComponent('Security');
		
		// spustit pouze pokud se jedna o JS ajax request
		if($this->request->is("ajax")){
			$this->viewBuilder()->autoLayout(false);
		}
		// pokud neni ajax
		if(!$this->request->is("ajax")){
			$this->loadComponent('MenuItem');
			$this->menuItems = $this->MenuItem->loadMenu();
		}

		// nastaveni cache response
		$this->response->withCache('-1 minutes','+5 days');

		// nastaveni promennt pro zavolani JS class z /js
		// $this->jsClass = [$this->request->params['controller'].ucFirst($this->request->params['action'])];
		// $this->set('jsClass', json_encode($this->jsClass));
		
		$this->set('controller', $this->request->params['controller']);
		$this->set('action', $this->request->params['action']);

		$this->redirectHttps();
	}
	
	/**
	 * inicializace reportingu chyb na rollbar.com
	 */
	private function initReporting()
	{
		$env = 'production';
	
		if (strpos($this->request->env('HTTP_HOST'),'localhost') > 0){
			$env = 'local';
		}
		if (strpos($this->request->env('HTTP_HOST'),'fastestdev.cz') > 0){
			$env = 'development';
		}

		setConfigure('ENV', $env);

		$monitoring = getConfigure('monitoring');
		$this->set('monitoringEnableLocal', $monitoring['enableLocal']); 

		if (($env != 'local' || $monitoring['enableLocal']) && !is_null($monitoring['post_server_item'])) {
			$this->set('monitoringAccessToken', $monitoring['post_client_item']);
		}
		$this->set('env', $env);
	}

	/**
     * presmerovani na https
     */
    private function redirectHttps(){
		// pr(env('HTTP_HOST'));die();
        if ((!env('HTTPS') || env('HTTPS') == 'off') && stripos(strrev(env('HTTP_HOST')), strrev('localhost')) !== 0 ) {
            header("HTTP/1.1 301 Moved Permanently");
            $redirectUrl = "https://" . env('HTTP_HOST') . env('REQUEST_URI');
            header("Location: $redirectUrl");
        }
    }

	/**
	 * 
	 * globalni funkce pro result v JSON
	 */
	public function jsonResult($result){
		$this->response->cache('-1 minutes','+5 days');
		$this->response->type('application/json');
		$this->response->body(json_encode($result));
		$this->autoRender = false;
		return  $this->response;	

	}

	/**
	 * 
	 * globalni funkce pro result v XML
	 */
	public function xmlResult($result){
		$this->response->cache('-1 minutes','+5 days');
		$this->response->type('text/xml');
		$this->response->body($result);
		$this->autoRender = false;
		return  $this->response;	
	}
	
	/**
	 * vypsani debug z mysql primo v controller pokud potrebuji
	 */
	public function debug(){
		$this->viewBuilder()->layout('debug');
		$this->render('../Pages/debug');
	}
	
	/**
	 * kontrola prihlaseni uzivatele a nastaveni promenne loggedUser
	 */
	private function loginComponent(){
		$this->loadComponent('Auth', [
			'loginAction' => [
				'controller' => 'Users',
				'action' => 'login'
			],
			
			'authError' => __('Nemáte přístup na tuto stránku'),
			'authenticate' => [
				'Form' => [
					'fields' => ['username' => 'email', 'password' => 'password'],	
				]
			]
		]);
		$this->Auth->allow();
		
		if ($this->Auth->user()){
			$this->set('loggedUser',$this->loggedUser = $this->Auth->user());
		}
	}
	
	/**
	 * chyba 404
	 */
	public function error404(){
		$this->response->statusCode(404);
		$this->set('title',__('Chyba 404 - stránka nenalezena'));
		
		return $this->render('/Pages/error404');
	}
	
	/**
	 * nacteni select config
	 */
	private function loadSelectConfig(){
		
		$this->set('articlePrefix', getConfigure('article_prefix'));

		$selectConfig = getConfigure('selectConfig');
		
		foreach($selectConfig AS $k=>$item){
			$this->$k = $item;
			$this->set($k,$item);
		}
		
		if (getConfigure('shop_selectConfig')){
		
			$selectConfig = getConfigure('shop_selectConfig');
			foreach($selectConfig AS $k=>$item){
				$this->$k = $item;
				$this->set($k,$item);
			}
		}
	}
	
	/**
	 * nacteni jazykovych mutaci
	 */
	private function loadLanguages(){
		$this->lang = 'cz';

		if ($this->request->getParam('lang') && !empty($this->request->getParams('lang'))){
			$this->lang = $this->request->params['lang'];
		}

		$this->set('lang',$this->lang);
		
		$this->url_lang_prefix = ((getConfigure('default_language') != $this->lang)?'/'.$this->lang:'');
		
		$this->set('url_lang_prefix',$this->url_lang_prefix);
		
		$this->Session->write('url_lang_prefix',$this->url_lang_prefix);
		$this->Session->write('lang',$this->lang);

		writeLocale($this->lang);
	}

	/**
	 * Before render callback.
	 *
	 * @param \Cake\Event\Event $event The beforeRender event.
	 * @return void
	 * @SuppressWarnings(PHPMD)
	 */
	public function beforeRender(Event $event)
	{
		if (!array_key_exists('_serialize', $this->viewVars) &&
			in_array($this->response->getType(), ['application/json', 'application/xml'])
		) {
			$this->set('_serialize', true);
		}
	}

	
	/**
	 * kontrola IE zda je mensi nez 10
	 */
	function checkIE(){
		preg_match('/MSIE (.*?);/', env('HTTP_USER_AGENT'), $matches);
		if(count($matches)<2){
		preg_match('/Trident\/\d{1,2}.\d{1,2}; rv:([0-9]*)/', env('HTTP_USER_AGENT'), $matches);
		}

		if (count($matches)>1){
		//Then we're using IE
		$version = intval($matches[1]);
		// pr($version);die();
		switch(true){
			case ($version<=10):
				$this->redirect('http://browsers.fastest.cz');
			//IE 8 or under!
			break;

			case ($version==9 || $version==10):
			//IE9 & IE10!
			break;

			case ($version==11):
			//Version 11!
			break;

			default:
			//You get the idea
		}
		}
	}

	/**
	 * nacteni settings z databaze
	 */
	private function loadSettings(){
		if (($settingLoad = getCache('settingLoad')) === false) {
			$this->loadModel('Settings');
			$settingLoad = $this->Settings->find()
				->select(['content'])
				->where(['type' => 'www'])
				->first();
		
			if ($settingLoad){
				$settingLoad = json_decode($settingLoad->content, true);
			}
			setCache('settingLoad', $settingLoad);
		}

		$this->setting = $settingLoad;
		$this->setting['page_title'] = $this->setting['title'];
		
		$this->set('setting',$this->setting);
		
	}
	
	/**
	 * nacteni JS a CSS + minifikace pokud je zapnuty compress
	 */
	private function loadJs() {
		// pokud je zapnuta koprimace nebo neexistuje gulpfile.json
		if (getConfigure('compress_html') || !file_exists('../gulpfile.json')){
			$this->set('compress',true);
			$jsUri = '/js/scripts.js';
			$cssUri = '/css/style-main.css';

			$this->set('jsUri',$jsUri.'?' . getConfigure('appVersion'));	
			$this->set('cssUri',$cssUri.'?' . getConfigure('appVersion'));
		}
		
		// pokud je vypnuta komprimace
		if (!getConfigure('compress_html') && file_exists('../gulpfile.json')){
			// $gulpFiles = json_decode(file_get_contents('../gulpfile.json'), true);
			$jsUri = '/js/scripts.js';
			$cssUri = '/css/style-main.css';

			$this->set('jsUri',$jsUri);	
			$this->set('cssUri',$cssUri);
		
		}
		
		$this->set('debug', getConfigure('debug'));
	}
}
