<?php

namespace App\Controller\Component;
use Cake\Controller\Component;
use Cake\ORM\Entity;
use Cake\Cache\Cache;

class MenuItemComponent extends Component
{

	/**
	 * nacteni menuItems
	 */
	public function loadMenu(){
		$controller = $this->_registry->getController();
			
		if (($this->menuItems = getCache('menuItems')) === false) {
			$controller->loadModel('MenuItems');
			$query = $controller->MenuItems->find('threaded')
			->where([])
			->cache(function ($query) {
				return 'loadMenuItems-' . md5(serialize($query->clause('where')).serialize($this->lang));
			});
			$this->menuItems = $query->toArray();
			setCache('menuItems', $this->menuItems);
		}

		if (($this->menuItemsPath = getCache('menuItemsPath')) === false) {
			$controller->loadModel('MenuItems');
			$this->menuItemsPath = $controller->MenuItems->getFullPath();
			setCache('menuItemsPath', $this->menuItemsPath);
		}

		$controller->set('menuItemsPath',$this->menuItemsPath);
		$controller->set('menuItems',$this->menuItems);
		return $this->menuItems;
	}
	

}