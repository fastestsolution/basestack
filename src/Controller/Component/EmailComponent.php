<?php

namespace App\Controller\Component;

use Cake\Mailer\Email;
use Cake\Controller\Component;
use Cake\ORM\Entity;
use App\View\Helper\FastestHelper;
use Cake\Mailer\TransportFactory;
/*
$data = [
	'Users_link'=>'fastest.cz',
];
$opt = [
	'to'=>$find->email,
	'fromEmail'=>'test2@fastest.cz',
	'fromName'=>'test',
	'subject'=>'test',
	'template_id'=>1,
	'data'=>$data,
	'template'=>'fastest',
];
$this->Email->send($opt);

{LOOP:people}
<b>{name}</b> {surname}<br />
{ENDLOOP:people}
				
*/

class EmailComponent extends Component
{
  public $fromEmail = "info@varimeprovas.cz";
  public $fromName = "Vaříme pro Vás";
	
	
  // enable debug template send
  var $debugTemplate = false;
  
  // set price format fields
  var $options = [
	'price_format'=>[
		'price_vat'
	]
  ];
  
  // send from template
  public function send(array $options = null){
	
	$this->controller = $this->_registry->getController();
	
	$this->options = array_merge_recursive($this->options,$options);
	$options = $this->setOptionsEmail($options);
	
	foreach($options['to'] AS $to){
		$email = new Email();
		
		// odesilatel
		$email->from($this->fromEmail, $this->fromName);
		
		// prijemce
		$email->to($to)->emailFormat('html');
		
		if (!isset($options['fromName'])){
			$options['fromName'] = $to;
		}
		// pokud je fromName
		$email->from((isset($options['fromEmail'])? $options['fromEmail'] : $to), $options['fromName']);
		
		// load template from db
		if (isset($options['template_id'])){
			$this->loadTemplate($options['template_id']);
		}
		// subject email
		$email->subject((isset($this->template_data)?$this->template_data['subject']:$options['subject']));
		
		
		// predani promenych do view
		$email->viewVars(['data'=>(isset($options['data'])?$options['data']:'')]);
		
		// pokud je nastaven jiny template jinak pouzi template Templates/Email/html/fastest.ctp
		$email->template((isset($options["template"]) ? $options["template"] : 'fastest'));
		
		$email->send();
	}

  }

  /**
   * nastaveni options
   */
  private function setOptionsEmail($options){
	  
	if (isset($options['data'])){
		$this->data = $options['data'];
	}
	
	// pokud jsou data ze sablony
	if (isset($this->template_data['content'])){
		$options['data'] = $this->template_data['content'];
	}

	if (!is_array($options['to'])){
		$options['to'] = [
			0=>$options['to'],
		];
	}
	return $options;
  }

  // nahrazeni textu pres class TemplateEngine
  private function replaceText($content){
	if (isset($this->data)){
		$engine = new TemplateEngine();
		
		$template = $engine->process($content, $this->data,$this->options);
		
		return $template;
	}
  }
	  
  // nacteni z template ID	  
  private function loadTemplate($emailTemplateId){
	  $controller = $this->_registry->getController();
	  $controller->loadModel('EmailTemplates');
	  $template = $controller->EmailTemplates->find()
		->where(['id'=>$emailTemplateId])
		->select(['name','content','subject'])
		->hydrate(false)
		->first();
		if (!$template){
			$controller->jsonResult(['r'=>false, 'm'=>'Není definována emailová šablona']);
		}
	  $template['subject'] = $this->replaceText($template['subject']);
	  $template['content'] = $this->replaceText($template['content']);
	 
	  if ($this->debugTemplate == true){
		pr($template);
		return false;
	  }
	  $this->template_data = $template;
  }




}

/**
<h1>{subtitle}</h1>
{LOOP:people}
<b>{name}</b> {surname}<br />
{ENDLOOP:people}
**/
class TemplateEngine {

	function showVariable($name) {
		$this->els = explode('.',$name);
		
		$this->value = [];
		
		// nalezeni dle pole napr. user.first_name
		$this->findRecursive($this->data);
		
		
		if (isset($this->value)){
			if (empty($this->value) && $this->value != 0){
				echo __('Nevyplněno');
			} 
			if (!empty($this->value) && $this->value == 0){
				// format price
				$this->priceFormat();
				echo ($this->value);
			}
			
		} 
		if (!isset($this->value)){
			echo '';
		}
		
	}
	function findRecursive($data){
		if(count($this->els) == 1){
			if (!isset($data[$this->els[0]])) {
				//pr($this->els);die('aa');
				unset($this->els);
			}
		}
		//pr($this->els);
		if (!isset($this->els)){
		
			foreach ($this->els AS $el){
				
				if (isset($data[$el])) {
					if (is_array($data[$el])){
						$this->findRecursive($data[$el]);
					}	
					if (!is_array($data[$el])){
						$this->value = $data[$el];
						$this->key = $el;
					}
				}
			}
		}
		if (!isset($this->els)){
			unset($this->value);
		}
	}
	
	function priceFormat(){
		$fastest = new FastestHelper(new \Cake\View\View());
		if (isset($this->options['price_format']) && !empty($this->options['price_format'])){
		
			if (in_array($this->key,$this->options['price_format'])){	
				$this->value = $fastest->price($this->value);

			}
		}
	}
	
	function wrap($element) {
		$this->stack[] = $this->data;
		foreach ($element as $k => $v) {
			$this->data[$k] = $v;
		}
	}
	function unwrap() {
		$this->data = array_pop($this->stack);
	}
	/**
	 * @SuppressWarnings(PHPMD)
	 */
	function run() {
		ob_start ();
		eval(func_get_arg(0));
		return ob_get_clean();
	}
	function process($template, $data,$options) {
		$this->options = $options;
		$this->data = $data;
		$this->stack = array();
		$template = str_replace('<', '<?php echo \'<\'; ?>', $template);
		
		$template = preg_replace('~\{(\w+)}~', '<?php $this->showVariable(\'$1\'); ?>', $template);
		$template = preg_replace('~\{(\w+[.]\w+)}~', '<?php $this->showVariable(\'$1\'); ?>', $template);
		$template = preg_replace('~\{LOOP:(\w+)\}~', '<?php foreach ($this->data[\'$1\'] as $ELEMENT): $this->wrap($ELEMENT); ?>', $template);
		$template = preg_replace('~\{ENDLOOP:(\w+)\}~', '<?php $this->unwrap(); endforeach; ?>', $template);
		$template = '?>' . $template;
		return $this->run($template);
	}
}