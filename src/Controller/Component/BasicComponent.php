<?php

namespace App\Controller\Component;
use Cake\Controller\Component;
use Cake\ORM\Entity;
use Cake\Cache\Cache;
use Cake\Core\Configure;

class BasicComponent extends Component
{

	
	/*** ENCODE LONG URL **/	
	public function encodeLongUrl($data){
		return strtr(base64_encode(addslashes(gzcompress(json_encode($data),9))), '+/=', '-_,');    
	}

   /*** DECODE LONG URL **/	
   public function decodeLongUrl($data){
	   $data2 = explode('?',$data);
	   $data = $data2[0];
	   return json_decode(@gzuncompress(stripslashes(base64_decode(strtr($data, '-_,', '+/=')))),true);
	}

}