<?php

namespace App\Controller\Component;

use Cake\Mailer\Email;
use Cake\Controller\Component;
use Cake\ORM\Entity;


class SmsComponent extends Component
{

	function send($user, $recipientList = array(), $message = ''){
				$output = array();
				//$message = 'Vaše objednávka je na ceste.';
				foreach($recipientList as $phoneNumber){
					$sms = '<?xml version="1.0" encoding="utf-8" ?> <batch id=""> <request>textSMS</request>  <recipient>+'.$phoneNumber.'</recipient> <content>'.$message.'</content>  <udh />  <delivery_report>0</delivery_report><custid>Lapet - '.$user.'</custid>   </batch>'; 			
					$context = stream_context_create( array( 'http' => array( 'method'=>"POST", 'Host' => 'apiserver', 'header'=>"Content-Type: text/xml", 'content'=>$sms ) ) );
					//pr($SMS);
					//die();
								
					$file = @file_get_contents('http://directsmsapi.sluzba.cz/apifastest/receiver.asp', false, $context);
					if (!$file){
						$output = array('r'=>false,'m'=>'Chyba odeslani '.$phoneNumber, 'phone'=>$phoneNumber);
						return $output;
					}
					$doc = simplexml_load_string($file);
					//pr($doc);
					switch ($doc->status){
						case '200':
							$output[] = array('r'=>true,'m'=>'SMS odeslána na tel. '.$phoneNumber);
							break;
						case '400':
							$output[] = array('r'=>false,'m'=>'Neplatné telefonní císlo', 'phone'=>$phoneNumber);
						default:
							$output[] = array('r'=>false,'m'=>'Neošetrená chyba pri odesílání SMS', 'phone'=>$phoneNumber);
							break;
					}
                                        @file_put_contents( ROOT . '/tmp/logs/sms_log.txt' .'.txt', date('Y-m-d H:i:s'). ' - ' . json_encode($output) . PHP_EOL, FILE_APPEND);
				}
				if (count($output) == 1){
					$output = $output[0];
				}
				return $output;
	}

}