<?php
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\I18n\Time;
use Cake\Cache\Cache;

class ArticlesController extends AppController
{
	var $articleFields = [
		'name',
		'alias',
		'id',
		'created',
		'content', 
	];

    public function beforeFilter(\Cake\Event\Event $event) {

        $this->viewBuilder()->layout('default');
        return parent::beforeFilter($event);
	}
	
	/**
	 * article index
	 */
    public function index(){
        $path = func_get_args();
		// pr($this->menuItems);die();
		// nacteni menu items a jejich ID dle url
		$this->menu_data = $this->Articles->MenuItems($path,$this->menuItems);
			
		// pokud nenalezeno tak 404
		if (!isset($this->menu_data['ids']) || count($this->menu_data['ids']) == 0){
			return $this->error404();
		}
		// pokud nenalezeno 1 clanek presmeruj na detail
		if (count($this->menu_data['ids']) == 1){
			return $this->detail('detail',$this->menu_data['ids'][0]);
		}
		
		$conditions = [
			'Articles.status' => 1,
			'Articles.id IN' => $this->menu_data['ids']
		];
		$conditionsHash = md5(json_encode($conditions));

		// nalezeni clanku dle zadaneho url
		if (($articles = getCache('articles_'.$conditionsHash)) === false) {	
			$articles = $this->Articles->find()
				->where($conditions)
				->select($this->articleFields)
				->order('Articles.id DESC')
				->cache(function($articles){
					return 'articles-' . md5(serialize($articles->clause('where')).serialize($articles->clause('order')));
				});
			;
			setCache('articles_'.$conditionsHash, $articles);
		}

		// zavolani pagination
		$items = $this->paginate($articles)->toArray();
		
		if (count($items) == 0){
			return $this->error404();
		}
		$this->set('items',$items);
		
		// drobeckova
		$this->set("breadcrumb",$this->menu_data['breadcrumb']);
		$this->menu_data['breadcrumb'] = reset($this->menu_data['breadcrumb']);
		
		// title
		$this->set("title", $this->menu_data['breadcrumb']);
		
		if ($this->request->is("ajax")){
			$this->viewBuilder()->autoLayout(false);
			$this->render('items');
		}
		
		$this->render('index');
		
    }
	
	/**
	 * detail clanku
	 */
	public function detail($alias = null, $articleId = null){
		$this->set('alias', $alias);
		$conditions = [
			'Articles.id' => $articleId,
			'Articles.status' => 1,
		];
		$conditionsHash = md5(json_encode($conditions));

		// nalezeni clanku
		if (($article = getCache('article_'.$conditionsHash)) === false) {	
			$article = $this->Articles->find()
				->where($conditions)
				->cache(function ($article) {
					return 'articles_id-' . md5(serialize($article->clause('where')).serialize($article->clause('order')));
			});
			$article = $article->first();
		
			setCache('article_'.$conditionsHash, $article);
		}
		
		// pokud nenalezeno tak 404
		if (!$article){
			return $this->error404();
		}
		
		// seo text
		$seo = [
			'title' => $article->name,
			'description' => (!empty($article->seo_description)?$article->seo_description:$article->content),
			'keywords' => (!empty($article->seo_keywords)?$article->seo_keywords:''),
		];
		if (!empty($article->images)){
			$images = json_decode($article->images,true);
			if (isset($images[0]['file'])){
				$seo['img'] = '/uploaded/articles/'.$images[0]['file'];
			}
		}
		
		$this->set('seo',$seo);
		$this->set("title", $article->name);
		$this->set('article',$article);
		$this->render('detail');
	}
	
	
	

	 
	
}
