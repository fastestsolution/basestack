<?php
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\I18n\Time;
use lessc\lessc;
use Cake\Cache\Cache;

class PagesController extends AppController
{

	/**
	 * homepage projektu
	 */
    public function homepage(){
		// $this->set('title',__('Homepage'));
		$this->jsClass = ['testFst'];
		$this->set('jsClass', json_encode($this->jsClass));
	}	
	
	/**
	 * sablona pro contact form
	 */
	private function contactFormTemplate(){
		$this->request->data['name'] = $this->request->data['email']['value'];
		$this->request->data['email'] = $this->request->data['email']['value'];
		
		if (isset($this->request->data['jmeno']['value']))
			$this->request->data['jmeno'] = $this->request->data['jmeno']['value'];
		if (isset($this->request->data['text']['value']))
			$this->request->data['text_tmp'] = $this->request->data['text']['value'];
		
		$html = '<table>';
		
		if (isset($this->request->data['email'])){
			$html .= '<tr><th>Email: </th><td>'.$this->request->data['email'].'</td></tr>';
			
		}
		if (isset($this->request->data['jmeno'])){
			$html .= '<tr><th>Jméno: </th><td>'.$this->request->data['jmeno'].'</td></tr>';
			
		}
		foreach($this->request->data AS $k=>$d){
			if (isset($d['name'])){
				$html .= '<tr><th>'.$d['name'].': </th><td>'.(!empty($d['value'])?$d['value']:'Nevyplněno').'</td></tr>';
				if (isset($this->request->data[$k]))
				unset($this->request->data[$k]);
			}
		}
		$html .= '</table>';
		
		$this->request->data['text'] = $html;
	}

	/**
	 * ulozeni kontaktniho formulare
	 */
	public function saveContactForm(){
		// pr($this->request);die('a');
		if ($this->request->is("ajax")){
			$this->loadModel('ContactForms');
			$contactForm = $this->ContactForms->newEntity();
			
			$html = $this->contactFormTemplate();
			
			$this->ContactForms->patchEntity($contactForm, $this->request->data());
			$this->ContactForms->checkError($contactForm);
			// send email
			$htmlEmail = '<h2>Email z kontaktního formuláře '.$this->request->env('HTTP_HOST');
			$htmlEmail .= $html;
				 
			$this->setting['email_robot'] = 'test@fastest.cz';
			$this->loadComponent('Email');
			$opt = [
				'to'=> [$this->setting['email_robot']], //$this->system_email,
				'subject'=>'Email z kontaktního formuláře '.$this->request->env('HTTP_HOST'),
				'data'=>$htmlEmail,
			];
			$this->Email->send($opt);
			
			// save form to db
			if ($this->ContactForms->save($contactForm)) {
				return $this->jsonResult(['result'=>true,'message'=>__('Formulář byl odeslán'),'clear'=>true]);
			}
			$this->jsonResult(['result'=>false,'message'=>__('Chyba uložení')]);			
		}
	}
	/**
	 * vymazani vsech cache
	 * 
	 * @SuppressWarnings(PHPMD)
	 */
	public function clearCache(){
		Cache::clear(false);
		$this->jsonResult(['result'=>true,'message'=>__('Cache smazana')]);
	}

	/**
	 * vyhledavani v ares
	 */
    public function ares($search = '28591232'){
       
        $this->loadComponent('Ares');
        $result = $this->Ares->search($search);
        
		$this->jsonResult($result);
    }

	/**
	 * google sitemap
	 * @SuppressWarnings(PHPMD)
	 */
	public function googleSitemap(){
		$this->autoRender = false;
		$sitemap = [];
		
		$this->loadModel('MenuItems');
		$sitemap = array_merge($sitemap,$this->MenuItems->getFullPath('google',((getConfigure('article_prefix') != '/') ? getConfigure('article_prefix') : '') ));
		
		// shop sitemap
		if (getConfigure('modul_shop')){
			$this->loadModel('ShopCategories');
			$sitemap = array_merge($sitemap,$this->ShopCategories->getFullPath('google'));
			
			$this->loadModel('Shop.ShopProducts');
			$sitemap = array_merge($sitemap, $this->ShopProducts->getFullPath());
		
		}
		$xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
		$xml .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
			if (isset($sitemap))
			foreach($sitemap as $s){
				$url = $s['url'];
				
				if (strpos($s['url'],'http://') !== false){
					$url = $s['url'];
				}
				if (strpos($s['url'],'http://') === false){
					$url ='http://'.$this->request->env('SERVER_NAME').$s['url'];
				}
				$xml .='
				<url>
					<loc>'.$url.'</loc>'.
                                        (!empty($s['modified']) ? 
                                            '<lastmod>'. $s['modified']->format('Y-m-d').'</lastmod>' : '') .
					'<changefreq>weekly</changefreq>
					<priority>'.(($s['url']!='/')?'0.6':'1').'</priority>
				</url>';
			}
		$xml .= '</urlset>';
		$this->xmlResult($xml);
	}
	
	/**
	 * nahravani souboru
	 * @SuppressWarnings(PHPMD)
	 */
	function fstUpload($delete = null){
		$fce = (isset($this->request->env['HTTP_X_FILENAME']) ? $this->request->env['HTTP_X_FILENAME'] : false);
		//pr($this->request->query);
		if (isset($this->request->query['params'])){
			$params = json_decode(base64_decode($this->request->query['params']));
		}
		
		$this->loadComponent('Upload');
		$params->file_ext = explode(',',$params->file_ext);
		//pr($params);
		
		if ($delete != null){
			$setting = array(
				'file'=>$params->file,
				'upload_path'=>$params->upload_path,
			);
			$this->Upload->delete_file($setting);
			
		
		}
		if (isset($_FILES['file']['type'])){
			
			$setting = array(
				'fn'=>(!isset($fce)?$fce:$_FILES['file']['name'][0]),
				'files'=>$_FILES,
				'filename'=>$_FILES['file']['name'][0],
				'tmp_name'=>$_FILES['file']['tmp_name'][0],
				'type'=>$_FILES['file']['type'][0],
				'file_ext'=>$params->file_ext,
				'count_file'=>$params->count_file,
				'upload_path'=>$params->upload_path,
			);
			if (isset($params->special_name) && !empty($params->special_name)){
				$setting['filename_type'] = 'special';
				$setting['filename_special'] = $params->special_name;
			}
			
			$this->Upload->doit($setting);
		} 
		if (isset($_FILES['file']['type'])){
			$this->jsonResult(['result'=>false,'message'=>'Chyba nahrání souboru']);
		}
	}
}
