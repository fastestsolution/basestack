# Base cake 3 with Bottstrap 4, Pure JS
Zakladni balicek pro webove projekty

## Obsahuje
---
- CakePHP 3.7
- Bootstrap 4
- Gulp 3.9
- Rollbar.com
- Webpack 4
- Bitbucket pipeline
- Export na AWS S3

## Instalace
---
1. upravit package.json nazev projektu
2. upravit package.json slack notifikaci nazev projektu
3. upravit bitbucket-pipelines.yml nazev projektu vsude kde je pouzit
4. nastavit rollbar config klice v /src/config/app.php v klici: **monitoring**
5. pri pouziti css pouzivat jednotlive komponenty vygenerovane v **/docs**
6. vytvorit /production/sqlSync/sqlConfig.json z sqlConfigDefault.json

```
composer install
npm install
gem install scss_lint
bundle install
```
spusteni gulp
```
gulp
```

pokud chci odeslat na produkcni prostredi 
```
npm run release
```

## Pro spravnou funkcnost nutno mit nainstalovano ve VSCode doplňky pro validaci kodu
---
https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint

https://marketplace.visualstudio.com/items?itemName=adamwalzer.scss-lint

https://github.com/brigade/scss-lint

https://marketplace.visualstudio.com/items?itemName=ecodes.vscode-phpmd


## Vytvoreni /src/config/db_config.php 
---
Pro pripojeni na DB je nutne do /src/config/db_config.php vytvorit:
```
 <?php
    return [
        'SQL_HOST'=>'localhost',
        'SQL_USERNAME'=>'root',
        'SQL_PASSWORD'=>'',
        'SQL_DATABASE'=>'pospiech'
    ];
    
```

## Vytvoreni tridy pro JS
---

do adresare /assets/js/components/ vytvorit NecoClass.js a nasledne naimportuj kde potrebujes
```
class Default {
	constructor() {
	}
}

export default new Default();

```

## Synchronizace databaze z staging nebo produkce do localhost
---
```
npm run sql
```
a vybrat moznost co chci udelat

## Migrace databaze
---
Pokud chci udelat zmeny v databazi spustim prvni script newMigration, ktery vytvori do /src/migrations/db/migrations/ soubor,  do ktereho je nutne zadat migracni script:
http://docs.phinx.org/en/latest/migrations.html
```
npm run newMigration -n TestMig
npm run applyMigration


```
## Create seed
```
npm run newSeed -n DefaultData
```

## Apply seeds
```
npm run applySeed -- -s DefaultData
```

## Uprava fstAlert a fstError
---
je pouzita externi knihovna https://sweetalert2.github.io fstAlert a fst Error jsou inicializovany v main.js, dle upravit dle dokumentace. Pokud chci upravit vzhled tak do css pridat napr:
```
.swal2-popup .swal2-title {
    color: red !important;
}
```

## FAQ
---

generator migraci
```
"odan/phinx-migrations-generator": "4.0.1"

```

Instalace ruby na UBUNTU
```
sudo apt-get install ruby-full build-essential
```

Automaticky makeFile maze vendors
```
    find $(root_project)/vendor/. -type f -name '.scrutinizer.yml' -delete
	find $(root_project)/vendor/. -type f -name '.travis.yml' -delete
	find $(root_project)/vendor/. -type f -name '*.dist' -delete
	find $(root_project)/vendor/. -type f -name 'composer.json' -delete
	find $(root_project)/vendor/. -type f -name 'composer.lock' -delete
	find $(root_project)/vendor/. -type f -name '.gitignore' -delete
	find $(root_project)/vendor/. -type f -name '.gitattributes' -delete
	find $(root_project)/vendor/. -type f -name 'LICENSE' -delete
	find $(root_project)/vendor/. -type f -name 'build.properties' -delete
	find $(root_project)/vendor/. -type f -name 'CHANGELOG' -delete
	find $(root_project)/vendor/. -type f -name 'AUTHORS.*' -delete
	find $(root_project)/vendor/. -type f -name '*.yml' -delete
	find $(root_project)/vendor/. -type f -name 'phpunit.*' -delete
	find $(root_project)/vendor/. -type f -name '.editorconfig' -delete
	find $(root_project)/vendor/. -type f -name 'pre-commit' -delete
	find $(root_project)/vendor/. -type f -name '.jshintrc' -delete
	find $(root_project)/vendor/. -type f -name 'Dockerfile' -delete
	find $(root_project)/vendor/. -type f -name '*.php4' -delete
	find $(root_project)/vendor/. -type f -name '*.md' -delete
	find $(root_project)/vendor/. -type f -name '*.sh' -delete
	find $(root_project)/vendor/. -type f -name 'README.*' -delete
	find $(root_project)/vendor/. -type f -name 'readme.*' -delete
	find $(root_project)/vendor/. -type f -name '*example.*' -delete
	find $(root_project)/vendor/. -type f -name '*example*.*' -delete
	find $(root_project)/vendor/. -type f -name 'contributing.*' -delete
	find $(root_project)/vendor/. -type f -name 'CONTRIBUTING.*' -delete
	find $(root_project)/vendor/. -type f -name '.php_cs' -delete
	rm -rf `find $(root_project)/vendor/. -type d -name Tests`
	rm -rf `find $(root_project)/vendor/. -type d -name examples`
	rm -rf `find $(root_project)/vendor/. -type d -name Demo`
	rm -rf `find $(root_project)/vendor/. -type d -name demo`
	rm -rf `find $(root_project)/vendor/phpunit/ -type d -name tests`
	rm -rf `find $(root_project)/vendor/odan/ -type d -name tests`
	rm -rf `find $(root_project)/vendor/monolog/ -type d -name tests`
	rm -rf `find $(root_project)/vendor/jdorn/ -type d -name tests`
	rm -rf `find $(root_project)/vendor/aptoma/ -type d -name tests`
	rm -rf `find $(root_project)/vendor/aura/ -type d -name tests`
	rm -rf `find $(root_project)/vendor/ajgl/ -type d -name tests`
	rm -rf `find $(root_project)/vendor/psy/ -type d -name tests`
	rm -rf `find $(root_project)/vendor/cakephp/bake/ -type d -name tests`
	rm -rf `find $(root_project)/vendor/cakephp/cakephp/ -type d -name tests`
	rm -rf `find $(root_project)/vendor/ -type d -name contrib`
	rm -rf `find $(root_project)/vendor/ -type d -name .github`
	rm -rf `find $(root_project)/vendor/ -type d -name bin`
	rm -rf `find $(root_project)/vendor/ -type d -name doc`
	rm -rf `find $(root_project)/vendor/ -type d -empty`
	rm -rf `find $(root_project)/vendor/ -type d -name twbs`
	rm -rf `find $(root_project)/vendor/ -type d -name sebastian`
	rm -rf `find $(root_project)/vendor/ -type d -name jakub-onderka`
	rm -rf `find $(root_project)/vendor/ -type d -name doctrine`
	rm -rf `find $(root_project)/vendor/ -type d -name aptoma`
	rm -rf `find $(root_project)/vendor/ -type d -name asm89`
	rm -rf `find $(root_project)/vendor/ -type d -name dnoegel`
	rm -rf `find $(root_project)/vendor/ -type d -name jalle19`
	rm -rf `find $(root_project)/vendor/ -type d -name jasny`
	rm -rf `find $(root_project)/vendor/ -type d -name jdorn`
	rm -rf `find $(root_project)/vendor/ -type d -name josegonzalez`
	rm -rf `find $(root_project)/vendor/ -type d -name justinrainbow`
	rm -rf `find $(root_project)/vendor/ -type d -name linkorb`
	rm -rf `find $(root_project)/vendor/ -type d -name m1`
	rm -rf `find $(root_project)/vendor/ -type d -name monolog`
	rm -rf `find $(root_project)/vendor/ -type d -name natxet`
	rm -rf `find $(root_project)/vendor/ -type d -name nervo`
	rm -rf `find $(root_project)/vendor/ -type d -name nikic`
	rm -rf `find $(root_project)/vendor/ -type d -name patchwork`
	rm -rf `find $(root_project)/vendor/ -type d -name pdepend`
	rm -rf `find $(root_project)/vendor/ -type d -name phpdocumentor`
	rm -rf `find $(root_project)/vendor/ -type d -name phpspec`
	rm -rf `find $(root_project)/vendor/ -type d -name phpunit`
	rm -rf `find $(root_project)/vendor/ -type d -name riimu`
	rm -rf `find $(root_project)/vendor/ -type d -name seld`
	rm -rf `find $(root_project)/vendor/ -type d -name squizlabs`
	rm -rf `find $(root_project)/vendor/ -type d -name tedivm`
	rm -rf `find $(root_project)/vendor/ -type d -name twbs`
	rm -rf `find $(root_project)/vendor/ -type d -name twig`
	rm -rf `find $(root_project)/vendor/ -type d -name umpirsky`
	rm -rf `find $(root_project)/vendor/ -type d -name webmozart`
	rm -rf `find $(root_project)/vendor/ -type d -name websharks`
	rm -rf `find $(root_project)/vendor/ -type d -name ajgl`
	rm -rf `find $(root_project)/vendor/symfony/ -type d -name console`
	rm -rf `find $(root_project)/vendor/symfony/ -type d -name yaml`
	rm -rf `find $(root_project)/vendor/symfony/ -type d -name debug`
	rm -rf `find $(root_project)/vendor/symfony/ -type d -name dependency-injection`
	rm -rf `find $(root_project)/vendor/symfony/ -type d -name filesystem`
	rm -rf `find $(root_project)/vendor/cakephp/ -type d -name migrations`
	rm -rf `find $(root_project)/vendor/cakephp/ -type d -name cakephp-codesniffer`

```