/**
 *  Copyright 2019 Jakub Tyson
 * 
 * SPUSTIT PRIKAZY: 
 * gulp - spusti naslouchani v browseru
 * gulp image -i /uploaded/image.jpg - spusti minifikaci jednoho obrazku, nutno rucne nahradit
 */

'use strict';
var autoprefixer = require('gulp-autoprefixer');
var csso = require('gulp-csso');
var del = require('del');
var gulp = require('gulp');
var runSequence = require('run-sequence');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var webpackStream = require('webpack-stream');
const imagemin = require('gulp-imagemin');
var browserSync = require('browser-sync').create();
var fs = require('fs');
const babel = require('gulp-babel');
const zip = require('gulp-zip');
const svgmin = require('gulp-svgmin');
const mergeStream = require('merge-stream');
const tap = require('gulp-tap');
const path = require('path');
const iconfont = require('gulp-iconfont');
const consolidate = require('gulp-consolidate');
const yargs = require('yargs');
const rename = require('gulp-rename');
const sassVars = require('gulp-sass-vars');
const hologram = require('gulp-hologram');
var jsonData = JSON.parse(fs.readFileSync('./gulpfile.json'));


const appVersion = (typeof (yargs.argv.appVersion) !== 'undefined') ? yargs.argv.appVersion : 'dev';
/**
 * UPRAVIT DLE PROJEKTU
 */
var appConfig = {
	bucketName: 'fastest-basestack', // url na kterem bezi localhost
	host: 'basestack.localhost', // url na kterem bezi localhost
	jsPath: 'js',
	cssPath: 'css',
	distPath: 'webroot/',
};

// definovani less souboru ktere budou slouceny
var stylesList = jsonData['stylesList'];

// definovani less souboru ktere se vygeneruji solo
var stylesListSolo = jsonData['stylesListSolo'];

// definovani JS souboru ktere se vygeneruji solo
var scriptsListSolo = jsonData['scriptsListSolo'];

//definovani JS souboru
var scriptsList = jsonData['scriptsList'];

// Set the browser that you want to supoprt
const AUTOPREFIXER_BROWSERS = [
	'ie >= 10',
	'ie_mob >= 10',
	'ff >= 30',
	'chrome >= 34',
	'safari >= 7',
	'opera >= 23',
	'ios >= 7',
	'android >= 4.4',
	'bb >= 10',
];

// vygenerovani solo stylu pro async nacitani
gulp.task('stylesSolo', function () {
	// return gulp.src('./src/sass/styles.scss')
	return gulp.src(stylesListSolo)
		// Compile SASS files
		.pipe(sass({
			outputStyle: 'nested',
			precision: 10,
			includePaths: ['.'],
			onError: console.error.bind(console, 'Sass error:'),
		}))
		// Auto-prefix css styles for cross browser compatibility
		.pipe(autoprefixer({browsers: AUTOPREFIXER_BROWSERS}))
		// Minify the file
		.pipe(csso())
		// Output
		.pipe(gulp.dest('./' + appConfig.distPath + '/css'))
		.pipe(browserSync.stream());
});

// vygenerovani solo JS pro async nacitani
gulp.task('scriptsSolo', function () {
	return gulp.src(scriptsListSolo)
		.pipe(babel({
				presets: ['@babel/env'],
		}))
		.pipe(uglify())
		// Output
		.pipe(gulp.dest('./' + appConfig.distPath + '/js'))
		.pipe(browserSync.stream());
});

// Gulp task to minify CSS files for DEV nominificate
gulp.task('stylesGen', function () {
	return gulp.src(stylesList)
		// Compile SASS files
		.pipe(sassVars({
			appVersion: appVersion,
		}))
		.pipe(sass({
			outputStyle: 'nested',
			precision: 10,
			includePaths: ['.'],
			onError: console.error.bind(console, 'Sass error:'),
		}))
		// Auto-prefix css styles for cross browser compatibility
		.pipe(autoprefixer({browsers: AUTOPREFIXER_BROWSERS}))
		// Minify the file
		.pipe(csso())
		// .pipe(concat('styles.css'))
		// Output
		.pipe(gulp.dest('./' + appConfig.distPath + '/css'))
		.pipe(browserSync.stream());
});

// Gulp task to minify JavaScript files no
gulp.task('scriptsGen', function() {
	return gulp.src(scriptsList)
		.pipe(babel(
			{
				// sourceType: 'script',
				presets: ['@babel/env'],
			}
		))
		.pipe(webpackStream(require('./production/webpack.config.js')))
		.pipe(concat('scripts.js'))
		// Output
		.pipe(gulp.dest('./' + appConfig.distPath + '/js'))

		.pipe(browserSync.stream());
});

// minifikace css image rucne na ceste
gulp.task('imagesPath', () => {
	/* eslint-disable no-console */
	if (!process.argv[4]){
		console.log('--------------');
		console.log('!!!!!!!!! Neexistuje atribut -i webroot/uploaded/...');
		console.log('---------------');
		return false;
	}
	var pathImg = './' + process.argv[4];
	console.log('Minifikace images na ceste: ' + pathImg);
	console.log('cilova cesta: ', path + '-min');
	gulp.src(pathImg + '/**/*')
	.pipe(imagemin())
	.pipe(gulp.dest(pathImg + '-min'));
});

// minifikace css image jednoho
gulp.task('image', () => {
	if (!process.argv[4]){
		console.log('--------------');
		console.log('!!!!!!!!! Neexistuje atribut -i webroot/uploaded/...');
		console.log('---------------');
		return false;
	}
	var pathImg = process.argv[4];
	var fileNamePath = pathImg.split('/');
	var fileName = fileNamePath[fileNamePath.length - 1];
	delete fileNamePath[fileNamePath.length - 1];
	var filePath = fileNamePath.join('/');

	var fileExt = fileName.split('.');

	console.log('Minifikace image na ceste: ' + filePath + fileName);
	console.log('cilova cesta: ', filePath + fileExt[0] + '-min.' + fileExt[1]);
	gulp.src(filePath + fileName)
	.pipe(imagemin())
	.pipe(gulp.dest(filePath + 'min'));
});

// Clean output directory
gulp.task('clean_css', () => del([appConfig.distPath + '/css']));
gulp.task('clean_js', () => del([appConfig.distPath + '/js']));



// Minify SVG.
gulp.task('svg-min', () => {
	del(['tmp/icons/**/*.svg']);
	return gulp.src('webroot/icons/**/*.svg')
		.pipe(svgmin())
		.pipe(gulp.dest('tmp/icons'));
});

gulp.task('icon-fonts', (callback) => {
	const fontName = 'icons';
	const runTimestamp = Math.round(Date.now() / 1000);

	const cssDocsFile = 'assets/scss/global/partials/_icons.scss';
	const cssDocs = fs.readFileSync(cssDocsFile, 'utf8');

	let streams = mergeStream();

	let svgStream = gulp.src(['tmp/icons/*.svg'])
		.pipe(tap((file) => {
			let extension = path.extname(file.path);
			let basename = path.basename(file.path, extension);


			if (cssDocs.indexOf(basename) <= -1) {
				console.log('WARNING: Documentation missing for icon: ' + basename);
			}
		}))
		.pipe(iconfont({
			fontName: fontName,
			prependUnicode: true,
			timestamp: runTimestamp,
			formats: ['ttf', 'eot', 'woff', 'woff2', 'svg'],
			normalize: true,
			fontHeight: 1000,
		}))
		.on('glyphs', (codepoints) => {
			let templateStream = gulp.src('assets/scss/global/partials/_icons.scss.erb')
				.pipe(consolidate('lodash', {
					glyphs: codepoints,
					fontName: fontName,
					fontPath: '../fonts/icons/',
					className: 'icon',
					version: appVersion,
				}))
				.pipe(rename((filePath) => {
					filePath.basename = fontName;
					filePath.extname = '.scss';
				})).pipe(gulp.dest('tmp/css/'));

			streams.add(templateStream);
		}).pipe(gulp.dest('webroot/fonts/icons'));

	streams.add(svgStream);

	return streams.on('close', callback);
});

// vytvoreni dokumentace ikony
gulp.task('icons-docs', () => {
	return gulp.src(['webroot/icons/**/*.svg'])
		.pipe(tap((file) => {
			const extension = path.extname(file.path);
			const basename = path.basename(file.path, extension);

			let html = '  <div class="col-xs-6 col-sm-4 col-lg-3">' + "\n";
			html += '    <div class="docs__icon">' + "\n";
			html += '      <span class="icon icon-' + basename + '"></span>' + "\n";
			html += '      <p>' + "\n";
			html += '        <code>.icon icon-' + basename + '</code>' + "\n";
			html += '      </p>' + "\n";
			html += '    </div>' + "\n";
			html += '  </div>' + "\n";

			console.log(html);
		}));
});

// run build generate icons
gulp.task('icons', (callback) => {
	return runSequence('svg-min', 'icon-fonts', callback);
});

// Documentation
gulp.task('hologram', () => {
	return gulp.src('docs/hologram/config.yml')
		.pipe(hologram({
			bundler: true,
			logging: true,
		}));
});


// gulp production run
gulp.task('production', () => {
	runSequence(
		'icons',
		'hologram',
		'clean_js',
		'clean_css',
		'stylesGen',
		'stylesSolo',
		'scriptsSolo',
		'scriptsGen'
	);
});

// synchronizace s browserem
gulp.task('browser-sync', ['production'], function() {
	browserSync.init({
			open: 'external',
			// open: false,
			host: appConfig.host,
			proxy: appConfig.host,
			// proxy: '192.168.33.33',
	});
	gulp.watch(['assets/scss/**/*.scss'], ['stylesGen', 'stylesSolo']).on('change', browserSync.reload);
	gulp.watch(stylesListSolo, ['stylesGen', 'stylesSolo']).on('change', browserSync.reload);
	gulp.watch(scriptsList, ['scriptsGen']).on('change', browserSync.reload);
	gulp.watch(scriptsListSolo, ['scriptsGen']).on('change', browserSync.reload);
});

gulp.task('package', () => {
	return gulp.src('production/build/**/*', {dot: true})
		.pipe(zip( appConfig.bucketName + '.zip'))
		.pipe(gulp.dest('production/target/'));
});

// Gulp task to minify all files
gulp.task('default',  () => {
	runSequence(
		'production',
		'browser-sync'
	);
});
