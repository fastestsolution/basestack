# ------------------------------------------------------------
# SCHEMA DUMP FOR TABLE: layout_positions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `layout_positions`;
CREATE TABLE `layout_positions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `system_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(60) NOT NULL,
  `key_name` varchar(60) NOT NULL,
  `hp_banner_queue` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `a` (`system_id`),
  KEY `key_name` (`key_name`)
) ENGINE = MyISAM DEFAULT CHARSET = utf8 ROW_FORMAT = DYNAMIC;

# ------------------------------------------------------------
# SCHEMA DUMP FOR TABLE: actuals
# ------------------------------------------------------------

DROP TABLE IF EXISTS `actuals`;
CREATE TABLE `actuals` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `alias` varchar(200) NOT NULL,
  `content` longtext NOT NULL,
  `images` text COMMENT 'JSON',
  `status` int(2) NOT NULL DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `trash` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `trash` (`trash`)
) ENGINE = MyISAM DEFAULT CHARSET = utf8 ROW_FORMAT = DYNAMIC;

# ------------------------------------------------------------
# SCHEMA DUMP FOR TABLE: articles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `articles`;
CREATE TABLE `articles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `system_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(200) NOT NULL,
  `alias` varchar(200) NOT NULL,
  `content` longtext NOT NULL,
  `images` text COMMENT 'JSON',
  `attachments` text COMMENT 'JSON',
  `seo_keywords` varchar(250) DEFAULT NULL,
  `seo_description` varchar(250) DEFAULT NULL,
  `additional_html` text,
  `status` int(2) NOT NULL DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `trash` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sid` (`system_id`),
  KEY `staus` (`status`),
  KEY `trash` (`trash`)
) ENGINE = MyISAM AUTO_INCREMENT = 2 DEFAULT CHARSET = utf8 ROW_FORMAT = DYNAMIC;

# ------------------------------------------------------------
# SCHEMA DUMP FOR TABLE: contact_forms
# ------------------------------------------------------------

DROP TABLE IF EXISTS `contact_forms`;
CREATE TABLE `contact_forms` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(200) NOT NULL DEFAULT '',
  `text` longtext NOT NULL,
  `status` int(2) NOT NULL DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `trash` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `trash` (`trash`)
) ENGINE = MyISAM AUTO_INCREMENT = 17 DEFAULT CHARSET = utf8 ROW_FORMAT = DYNAMIC;

# ------------------------------------------------------------
# SCHEMA DUMP FOR TABLE: email_templates
# ------------------------------------------------------------

DROP TABLE IF EXISTS `email_templates`;
CREATE TABLE `email_templates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  `subject` varchar(150) NOT NULL,
  `content` text NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `trash` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `trash` (`trash`)
) ENGINE = MyISAM DEFAULT CHARSET = utf8 ROW_FORMAT = DYNAMIC;

# ------------------------------------------------------------
# SCHEMA DUMP FOR TABLE: email_templates_i18n
# ------------------------------------------------------------

DROP TABLE IF EXISTS `email_templates_i18n`;
CREATE TABLE `email_templates_i18n` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `locale` varchar(6) NOT NULL,
  `model` varchar(80) NOT NULL,
  `foreign_key` int(10) unsigned NOT NULL,
  `field` varchar(120) NOT NULL,
  `content` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `locale` (`locale`, `model`, `foreign_key`, `field`),
  KEY `model` (`model`, `foreign_key`, `field`)
) ENGINE = MyISAM DEFAULT CHARSET = utf8 ROW_FORMAT = DYNAMIC;

# ------------------------------------------------------------
# SCHEMA DUMP FOR TABLE: i18n
# ------------------------------------------------------------

DROP TABLE IF EXISTS `i18n`;
CREATE TABLE `i18n` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `locale` varchar(6) NOT NULL,
  `model` varchar(80) NOT NULL,
  `foreign_key` int(10) unsigned NOT NULL,
  `field` varchar(120) NOT NULL,
  `content` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `locale` (`locale`, `model`, `foreign_key`, `field`),
  KEY `model` (`model`, `foreign_key`, `field`)
) ENGINE = MyISAM DEFAULT CHARSET = utf8 ROW_FORMAT = DYNAMIC;

# ------------------------------------------------------------
# SCHEMA DUMP FOR TABLE: article_menu_items
# ------------------------------------------------------------

DROP TABLE IF EXISTS `article_menu_items`;
CREATE TABLE `article_menu_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `article_id` int(10) unsigned NOT NULL,
  `menu_item_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `article_id` (`article_id`, `menu_item_id`)
) ENGINE = MyISAM AUTO_INCREMENT = 2 DEFAULT CHARSET = utf8 ROW_FORMAT = FIXED;

# ------------------------------------------------------------
# SCHEMA DUMP FOR TABLE: menu_items
# ------------------------------------------------------------

DROP TABLE IF EXISTS `menu_items`;
CREATE TABLE `menu_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `system_id` int(10) unsigned DEFAULT NULL,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `level` int(5) unsigned NOT NULL DEFAULT '1',
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  `name` varchar(80) DEFAULT '',
  `alias` varchar(100) DEFAULT NULL,
  `description` text,
  `spec_url` varchar(160) DEFAULT NULL,
  `hidden` int(1) NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `hidden` (`hidden`),
  KEY `parent_id` (`parent_id`),
  KEY `sid` (`system_id`)
) ENGINE = MyISAM AUTO_INCREMENT = 2 DEFAULT CHARSET = utf8 ROW_FORMAT = DYNAMIC;

# ------------------------------------------------------------
# SCHEMA DUMP FOR TABLE: phinxlog
# ------------------------------------------------------------

DROP TABLE IF EXISTS `phinxlog`;
CREATE TABLE `phinxlog` (
  `version` bigint(20) NOT NULL,
  `migration_name` varchar(100) DEFAULT NULL,
  `start_time` timestamp NULL DEFAULT NULL,
  `end_time` timestamp NULL DEFAULT NULL,
  `breakpoint` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`version`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8 ROW_FORMAT = COMPACT;

# ------------------------------------------------------------
# SCHEMA DUMP FOR TABLE: settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `settings`;
CREATE TABLE `settings` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `system_id` int(10) unsigned DEFAULT NULL,
  `type` varchar(15) NOT NULL,
  `content` text COMMENT 'JSON',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `d` (`system_id`),
  KEY `type` (`type`)
) ENGINE = MyISAM AUTO_INCREMENT = 4 DEFAULT CHARSET = utf8 ROW_FORMAT = DYNAMIC;

# ------------------------------------------------------------
# SCHEMA DUMP FOR TABLE: smallboxes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `smallboxes`;
CREATE TABLE `smallboxes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `system_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `content` text NOT NULL,
  `layout_position_id` int(10) unsigned DEFAULT NULL,
  `background_img` text COMMENT 'JSON',
  `discount_box` text COMMENT 'JSON',
  `text_position_id` int(10) unsigned DEFAULT NULL,
  `text_color` varchar(12) DEFAULT NULL,
  `shop_category_id` int(10) unsigned DEFAULT NULL,
  `status` int(2) NOT NULL DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `trash` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fv` (`system_id`),
  KEY `layout_position_key` (`layout_position_id`),
  KEY `shop_category_id` (`shop_category_id`),
  KEY `status` (`status`),
  KEY `trash` (`trash`)
) ENGINE = MyISAM DEFAULT CHARSET = utf8 ROW_FORMAT = DYNAMIC;

# ------------------------------------------------------------
# SCHEMA DUMP FOR TABLE: user_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_groups`;
CREATE TABLE `user_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL,
  `permissions` text COMMENT 'JSON',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `trash` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `trash` (`trash`)
) ENGINE = MyISAM DEFAULT CHARSET = utf8 ROW_FORMAT = DYNAMIC;

# ------------------------------------------------------------
# SCHEMA DUMP FOR TABLE: users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_group_id` int(10) unsigned NOT NULL,
  `username` varchar(60) NOT NULL,
  `password` varchar(60) NOT NULL,
  `first_name` varchar(40) NOT NULL,
  `last_name` varchar(60) NOT NULL,
  `email` varchar(120) NOT NULL,
  `avatar` text COMMENT 'JSON',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `trash` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `trash` (`trash`),
  KEY `user_group_id` (`user_group_id`)
) ENGINE = MyISAM DEFAULT CHARSET = utf8 ROW_FORMAT = DYNAMIC; # ------------------------------------------------------------
# DATA DUMP FOR TABLE: layout_positions
# ------------------------------------------------------------


# ------------------------------------------------------------
# DATA DUMP FOR TABLE: actuals
# ------------------------------------------------------------


# ------------------------------------------------------------
# DATA DUMP FOR TABLE: articles
# ------------------------------------------------------------

INSERT INTO
  `articles` (
    `id`,
    `system_id`,
    `name`,
    `alias`,
    `content`,
    `images`,
    `attachments`,
    `seo_keywords`,
    `seo_description`,
    `additional_html`,
    `status`,
    `created`,
    `modified`,
    `trash`
  )
VALUES
  (
    1,
    NULL,
    'test',
    'test',
    'test',
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    1,
    NULL,
    NULL,
    NULL
  );

# ------------------------------------------------------------
# DATA DUMP FOR TABLE: contact_forms
# ------------------------------------------------------------

INSERT INTO
  `contact_forms` (
    `id`,
    `email`,
    `text`,
    `status`,
    `created`,
    `modified`,
    `trash`
  )
VALUES
  (
    1,
    'vfd',
    '<table><tr><th>Email: </th><td>vfd</td></tr><tr><th>Jméno: </th><td>vfd</td></tr><tr><th>Telefon: </th><td>Nevyplněno</td></tr><tr><th>Zpráva: </th><td>vfd</td></tr></table>',
    1,
    '2019-03-20 03:59:03',
    '2019-03-20 03:59:03',
    NULL
  );
INSERT INTO
  `contact_forms` (
    `id`,
    `email`,
    `text`,
    `status`,
    `created`,
    `modified`,
    `trash`
  )
VALUES
  (
    2,
    'vfd',
    '<table><tr><th>Email: </th><td>vfd</td></tr><tr><th>Jméno: </th><td>vfd</td></tr><tr><th>Telefon: </th><td>Nevyplněno</td></tr><tr><th>Zpráva: </th><td>vfd</td></tr></table>',
    1,
    '2019-03-20 04:00:01',
    '2019-03-20 04:00:01',
    NULL
  );
INSERT INTO
  `contact_forms` (
    `id`,
    `email`,
    `text`,
    `status`,
    `created`,
    `modified`,
    `trash`
  )
VALUES
  (
    3,
    'vfd',
    '<table><tr><th>Email: </th><td>vfd</td></tr><tr><th>Jméno: </th><td>vfd</td></tr><tr><th>Telefon: </th><td>Nevyplněno</td></tr><tr><th>Zpráva: </th><td>vfd</td></tr></table>',
    1,
    '2019-03-20 04:00:53',
    '2019-03-20 04:00:53',
    NULL
  );
INSERT INTO
  `contact_forms` (
    `id`,
    `email`,
    `text`,
    `status`,
    `created`,
    `modified`,
    `trash`
  )
VALUES
  (
    4,
    'vd',
    '<table><tr><th>Email: </th><td>vd</td></tr><tr><th>Jméno: </th><td>vfd</td></tr><tr><th>Telefon: </th><td>Nevyplněno</td></tr><tr><th>Zpráva: </th><td>Nevyplněno</td></tr></table>',
    1,
    '2019-03-20 04:12:46',
    '2019-03-20 04:12:46',
    NULL
  );
INSERT INTO
  `contact_forms` (
    `id`,
    `email`,
    `text`,
    `status`,
    `created`,
    `modified`,
    `trash`
  )
VALUES
  (
    5,
    'vd',
    '<table><tr><th>Email: </th><td>vd</td></tr><tr><th>Jméno: </th><td>vfd</td></tr><tr><th>Telefon: </th><td>Nevyplněno</td></tr><tr><th>Zpráva: </th><td>Nevyplněno</td></tr></table>',
    1,
    '2019-03-20 04:13:16',
    '2019-03-20 04:13:16',
    NULL
  );
INSERT INTO
  `contact_forms` (
    `id`,
    `email`,
    `text`,
    `status`,
    `created`,
    `modified`,
    `trash`
  )
VALUES
  (
    6,
    'vd',
    '<table><tr><th>Email: </th><td>vd</td></tr><tr><th>Jméno: </th><td>vfd</td></tr><tr><th>Telefon: </th><td>Nevyplněno</td></tr><tr><th>Zpráva: </th><td>Nevyplněno</td></tr></table>',
    1,
    '2019-03-20 04:13:52',
    '2019-03-20 04:13:52',
    NULL
  );
INSERT INTO
  `contact_forms` (
    `id`,
    `email`,
    `text`,
    `status`,
    `created`,
    `modified`,
    `trash`
  )
VALUES
  (
    7,
    'vd',
    '<table><tr><th>Email: </th><td>vd</td></tr><tr><th>Jméno: </th><td>vfd</td></tr><tr><th>Telefon: </th><td>Nevyplněno</td></tr><tr><th>Zpráva: </th><td>Nevyplněno</td></tr></table>',
    1,
    '2019-03-20 04:14:31',
    '2019-03-20 04:14:31',
    NULL
  );
INSERT INTO
  `contact_forms` (
    `id`,
    `email`,
    `text`,
    `status`,
    `created`,
    `modified`,
    `trash`
  )
VALUES
  (
    8,
    'vd',
    '<table><tr><th>Email: </th><td>vd</td></tr><tr><th>Jméno: </th><td>vfd</td></tr><tr><th>Telefon: </th><td>Nevyplněno</td></tr><tr><th>Zpráva: </th><td>Nevyplněno</td></tr></table>',
    1,
    '2019-03-20 04:14:42',
    '2019-03-20 04:14:42',
    NULL
  );
INSERT INTO
  `contact_forms` (
    `id`,
    `email`,
    `text`,
    `status`,
    `created`,
    `modified`,
    `trash`
  )
VALUES
  (
    9,
    'vd',
    '<table><tr><th>Email: </th><td>vd</td></tr><tr><th>Jméno: </th><td>vfd</td></tr><tr><th>Telefon: </th><td>Nevyplněno</td></tr><tr><th>Zpráva: </th><td>Nevyplněno</td></tr></table>',
    1,
    '2019-03-20 04:14:58',
    '2019-03-20 04:14:58',
    NULL
  );
INSERT INTO
  `contact_forms` (
    `id`,
    `email`,
    `text`,
    `status`,
    `created`,
    `modified`,
    `trash`
  )
VALUES
  (
    10,
    'vdf',
    '<table><tr><th>Email: </th><td>vdf</td></tr><tr><th>Jméno: </th><td>dvfd</td></tr><tr><th>Telefon: </th><td>Nevyplněno</td></tr><tr><th>Zpráva: </th><td>Nevyplněno</td></tr></table>',
    1,
    '2019-03-20 04:18:51',
    '2019-03-20 04:18:51',
    NULL
  );
INSERT INTO
  `contact_forms` (
    `id`,
    `email`,
    `text`,
    `status`,
    `created`,
    `modified`,
    `trash`
  )
VALUES
  (
    11,
    'vfd',
    '<table><tr><th>Email: </th><td>vfd</td></tr><tr><th>Jméno: </th><td>vfd</td></tr><tr><th>Telefon: </th><td>Nevyplněno</td></tr><tr><th>Zpráva: </th><td>Nevyplněno</td></tr></table>',
    1,
    '2019-03-20 04:19:36',
    '2019-03-20 04:19:36',
    NULL
  );
INSERT INTO
  `contact_forms` (
    `id`,
    `email`,
    `text`,
    `status`,
    `created`,
    `modified`,
    `trash`
  )
VALUES
  (
    12,
    'aa',
    '<table><tr><th>Email: </th><td>aa</td></tr><tr><th>Jméno: </th><td>aa</td></tr><tr><th>Telefon: </th><td>Nevyplněno</td></tr><tr><th>Zpráva: </th><td>Nevyplněno</td></tr></table>',
    1,
    '2019-03-20 05:30:55',
    '2019-03-20 05:30:55',
    NULL
  );
INSERT INTO
  `contact_forms` (
    `id`,
    `email`,
    `text`,
    `status`,
    `created`,
    `modified`,
    `trash`
  )
VALUES
  (
    13,
    'vdf',
    '<table><tr><th>Email: </th><td>vdf</td></tr><tr><th>Jméno: </th><td>vd</td></tr><tr><th>Telefon: </th><td>Nevyplněno</td></tr><tr><th>Zpráva: </th><td>Nevyplněno</td></tr></table>',
    1,
    '2019-03-20 05:31:14',
    '2019-03-20 05:31:14',
    NULL
  );
INSERT INTO
  `contact_forms` (
    `id`,
    `email`,
    `text`,
    `status`,
    `created`,
    `modified`,
    `trash`
  )
VALUES
  (
    14,
    'aa',
    '<table><tr><th>Email: </th><td>aa</td></tr><tr><th>Jméno: </th><td>aa</td></tr><tr><th>Telefon: </th><td>Nevyplněno</td></tr><tr><th>Zpráva: </th><td>Nevyplněno</td></tr></table>',
    1,
    '2019-03-20 05:33:28',
    '2019-03-20 05:33:28',
    NULL
  );
INSERT INTO
  `contact_forms` (
    `id`,
    `email`,
    `text`,
    `status`,
    `created`,
    `modified`,
    `trash`
  )
VALUES
  (
    15,
    'a',
    '<table><tr><th>Email: </th><td>a</td></tr><tr><th>Jméno: </th><td>a</td></tr><tr><th>Telefon: </th><td>Nevyplněno</td></tr><tr><th>Zpráva: </th><td>Nevyplněno</td></tr></table>',
    1,
    '2019-03-20 05:35:11',
    '2019-03-20 05:35:11',
    NULL
  );
INSERT INTO
  `contact_forms` (
    `id`,
    `email`,
    `text`,
    `status`,
    `created`,
    `modified`,
    `trash`
  )
VALUES
  (
    16,
    'a',
    '<table><tr><th>Email: </th><td>a</td></tr><tr><th>Jméno: </th><td>a</td></tr><tr><th>Telefon: </th><td>Nevyplněno</td></tr><tr><th>Zpráva: </th><td>Nevyplněno</td></tr></table>',
    1,
    '2019-03-20 05:35:15',
    '2019-03-20 05:35:15',
    NULL
  );

# ------------------------------------------------------------
# DATA DUMP FOR TABLE: email_templates
# ------------------------------------------------------------


# ------------------------------------------------------------
# DATA DUMP FOR TABLE: email_templates_i18n
# ------------------------------------------------------------


# ------------------------------------------------------------
# DATA DUMP FOR TABLE: i18n
# ------------------------------------------------------------


# ------------------------------------------------------------
# DATA DUMP FOR TABLE: article_menu_items
# ------------------------------------------------------------

INSERT INTO
  `article_menu_items` (`id`, `article_id`, `menu_item_id`)
VALUES
  (1, 1, 1);

# ------------------------------------------------------------
# DATA DUMP FOR TABLE: menu_items
# ------------------------------------------------------------

INSERT INTO
  `menu_items` (
    `id`,
    `system_id`,
    `parent_id`,
    `level`,
    `lft`,
    `rght`,
    `name`,
    `alias`,
    `description`,
    `spec_url`,
    `hidden`,
    `created`,
    `modified`
  )
VALUES
  (
    1,
    NULL,
    NULL,
    1,
    1,
    2,
    'test',
    'test',
    NULL,
    NULL,
    0,
    NULL,
    NULL
  );

# ------------------------------------------------------------
# DATA DUMP FOR TABLE: phinxlog
# ------------------------------------------------------------

INSERT INTO
  `phinxlog` (
    `version`,
    `migration_name`,
    `start_time`,
    `end_time`,
    `breakpoint`
  )
VALUES
  (
    20190220021038,
    'Default',
    '2019-02-20 03:10:38',
    '2019-02-20 03:10:38',
    0
  );
INSERT INTO
  `phinxlog` (
    `version`,
    `migration_name`,
    `start_time`,
    `end_time`,
    `breakpoint`
  )
VALUES
  (
    20190325021930,
    'Test',
    '2019-03-25 03:19:31',
    '2019-03-25 03:19:31',
    0
  );
INSERT INTO
  `phinxlog` (
    `version`,
    `migration_name`,
    `start_time`,
    `end_time`,
    `breakpoint`
  )
VALUES
  (
    20190402035311,
    'TestMig',
    '2019-04-02 05:53:40',
    '2019-04-02 05:53:40',
    0
  );

# ------------------------------------------------------------
# DATA DUMP FOR TABLE: settings
# ------------------------------------------------------------

INSERT INTO
  `settings` (
    `id`,
    `system_id`,
    `type`,
    `content`,
    `created`,
    `modified`
  )
VALUES
  (
    1,
    1,
    'www',
    '{\"title\":\"Motopark Ostrava\",\"keywords\":\"motopark, yamaha, motorky, honda\",\"google_analytics\":\"UA-77335645-40\",\"google_tag_id\":\"AW-814824809\",\"google_conversion_id\":\"833305523\",\"fa_ucet\":\"12-3456789\\/0100\",\"fa_vs_pref\":\"123\",\"panel_1_img\":\"banner-1.jpg\",\"panel_2_img\":\"banner-2.jpg\",\"panel_3_img\":\"banner-3.jpg\",\"panel_1_width\":350,\"panel_2_width\":320,\"panel_3_width\":290,\"mainpage_banner1\":[{\"file\":\"baner-panske-4.jpg\",\"name\":\"BANER-PANSKE-4.jpg\",\"height\":\"450\"}],\"mainpage_banner2\":[{\"file\":\"baner-damske-4.jpg\",\"name\":\"BANER-DAMSKE-4.jpg\",\"height\":\"450\"}],\"mainpage_banner3\":[{\"file\":\"baner-detske.jpg\",\"name\":\"BANER-DETSKE.jpg\",\"height\":\"450\"}],\"mainpage_banner4\":[{\"file\":\"hodiny-1920x450.jpg\",\"name\":\"HODINY-1920X450.jpg\",\"height\":\"450\"}],\"mainpage_banner5\":[{\"file\":\"kapesni-2.jpg\",\"name\":\"KAPESNI-2.jpg\",\"height\":\"450\"}],\"mainpage_banner_trash\":[],\"description\":\"Jsme Motopark Ostrava nejv\\u011bt\\u0161\\u00ed motork\\u00e1\\u0159sk\\u00e9 centrum na Morav\\u011b. Z\\u00e1ruka komplexn\\u00edch slu\\u017eeb, spokojenosti a z\\u00e1bavy. Na\\u0161im c\\u00edlem je poskytnout z\\u00e1kazn\\u00edk\\u016fm maxim\\u00e1ln\\u00ed p\\u00e9\\u010di a v\\u0161e na jednom m\\u00edst\\u011b.\",\"email_robot\":\"kolenovsky@fastest.cz\",\"email_robot_name\":\"Fastest.cz\",\"company_address\":\"Pobialova 14, Ostrava\",\"fb_pixel_id\":\"546228442376717\",\"google_analytics_global\":\"_aSiu--ZlkeA1WxznHqnJ4nNv4HbVYDYP7MIaIQzNlM\"}',
    NULL,
    '2018-08-02 11:06:12'
  );

# ------------------------------------------------------------
# DATA DUMP FOR TABLE: smallboxes
# ------------------------------------------------------------


# ------------------------------------------------------------
# DATA DUMP FOR TABLE: user_groups
# ------------------------------------------------------------


# ------------------------------------------------------------
# DATA DUMP FOR TABLE: users
# ------------------------------------------------------------