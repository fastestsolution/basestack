const fs = require('fs');
const mysqldump = require('mysqldump');
const List = require('prompt-list');
// var shell = require('shelljs');

const configSqlContent = fs.readFileSync('./production/sqlSync/sqlConfig.json');
const configSql = JSON.parse(configSqlContent);

var envTypeTo = 'local';
var envTypeFrom = 'local';

var envTypeFile = {
	local: 'dumpLocal',
	dev: 'dumpStaging',
	prod: 'dumpProduction',
};

var list = new List({
  name: 'syncType',
  message: 'Co chcete spustit za synchronizaci SQL do local DB?',
  choices: [
	{name: 'onlyDumpLocal'},
	{name: 'stagingToLocal'},
	{name: 'productionToLocal'}
  ]
});

/**
 * vytvoreni dump
 */
function createDump(){
	return new Promise((resolve, reject) => {
		const result = mysqldump({
			connection: {
				host: configSql[envTypeFrom].relace,
				user: configSql[envTypeFrom].username,
				password: configSql[envTypeFrom].password,
				database: configSql[envTypeFrom].database,
			},
			dump: {
				schema: {
					table: {
						ifNotExist: true,
						dropIfExist: true,
					},
				},
			},
			// dumpToFile: './' + envTypeFile[envTypeFrom] + '.sql',
		});

		result.then(res =>{
			// console.log(res.dump.data);
			let saveData = res.dump.schema + " " + res.dump.data;
			fs.writeFile('./' + envTypeFile[envTypeFrom] + '.sql', saveData, function(err) {
				if(err) {
					reject(err);
					return console.log(err);
				}
				resolve("The file './" + envTypeFile[envTypeFrom] + ".sql' was saved!");
				// console.log("The file './" + envTypeFile[envTypeFrom] + ".sql' was saved!");
			});
		});
	});
}

/**
 * obnoveni dump
 */
function restoreDump(){
	require('mysql-import').config({
		host: configSql[envTypeTo].relace,
		user: configSql[envTypeTo].username,
		password: configSql[envTypeTo].password,
		database: configSql[envTypeTo].database,
		onerror: err=>console.log(err.message)

	}).import('./' + envTypeFile[envTypeFrom] + '.sql').then(()=> {
		console.log('dump SQL byl obnoven ./' + envTypeFile[envTypeFrom] + '.sql do LOCAL DB');
	});
}

/**
 * spusteni tasku dle vyberu
 */
list.ask(function(type) {

	// dump localhost
	if (type == 'onlyDumpLocal'){
		envTypeTo = 'local';
		envTypeFrom = 'local';

		// odkomentovat co chcete delat s lokalni DB

		// createDump().then(res=>{
		// 	console.log('RESULT: ', res);
		// });

		restoreDump();
	}

	// dump staging to localhost
	if (type == 'stagingToLocal'){
		envTypeTo = 'local';
		envTypeFrom = 'dev';

		createDump().then(res =>{
			console.log('RESULT: ', res);
			restoreDump();
		});
	}

	// dump production to localhost
	if (type == 'productionToLocal'){
		envTypeTo = 'local';
		envTypeFrom = 'prod';

		createDump().then(res =>{
			console.log('RESULT: ', res);
			restoreDump();
		});
	}

});
