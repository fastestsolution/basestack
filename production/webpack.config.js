
const Webpack = require('webpack');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const fs = require('fs');
const Path = require('path');

var jsonData = JSON.parse(fs.readFileSync('./gulpfile.json'));

module.exports = {
	mode: 'production',
	entry: {
		scripts: jsonData.scriptsList,
	},
	output: {
		path: Path.join(__dirname, '../webroot/dist/'),
		filename: 'dist/js/[name].js',
		chunkFilename: 'js/[name].js',
	},
	optimization: {
		splitChunks: {
			chunks: 'all',
			name: false,
		},
	},
	plugins: [
		new Webpack.optimize.ModuleConcatenationPlugin(),
		new CleanWebpackPlugin(['webroot/dist/js'], { root: Path.resolve(__dirname, '..') }),
	],
	module: {
		rules: [
			{
				test: /\.(js)$/,
				exclude: /node_modules/,
				use: 'babel-loader',
			},
		],
	},
};
