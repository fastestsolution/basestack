<?php
// kontrola zda je definovano prostredi
if (!in_array($_ENV['CONFIG_ENV'], ['PROD','DEV'])){
    echo '!!!!!!! Neni definovano prostredi pro BUILD !!!!!!!!';
    throw new Exception('Neni definovano prostredi pro BUILD');

} else {
    $configList = [
        'SQL_HOST_DEV_PIPELINE',
        'SQL_HOST_DEV',
        'SQL_NAME_DEV',
        'SQL_DB_DEV',
        'SQL_PASSWORD_DEV',
        'SQL_HOST_PROD_PIPELINE',
        'SQL_HOST_PROD',
        'SQL_NAME_PROD',
        'SQL_DB_PROD',
        'SQL_PASSWORD_PROD',
    ];
    $notDefine = [];
    foreach($configList AS $con){
        if (!isset($_ENV[$con])){
            $notDefine[] = $con;
        }
    }
    if (!empty($notDefine)){
        echo 'V bitbucket repository nejsou definovany v settings nektere promenne, upravte je a spuste pipeline znovu !';
        throw new Exception("Neni definovana promenna v prostredi: \n".implode("\n", $notDefine));
  
    }

    // pokud je pipeline na dev server
    if ($_ENV['CONFIG_ENV'] == 'DEV'){
        $config = [
            "paths" => [
                "migrations" => "./src/migrations/db/migrations",
                "seeds" =>  "./src/migrations/db/seeds"
            ],
            "environments" => [
                "default_migration_table" => "phinxlog",
                "default_database" => "development",
                "development" => [
                    "adapter" => "mysql",
                    "host" => $_ENV['SQL_HOST_DEV_PIPELINE'],
                    "name" => $_ENV['SQL_DB_DEV'],
                    "user" => $_ENV['SQL_NAME_DEV'],
                    "pass" => $_ENV['SQL_PASSWORD_DEV'],
                    "port" => 3306
                ]
            ]
        ];
    }

    // pokud je pipeline na produkcni server
    if ($_ENV['CONFIG_ENV'] == 'PROD'){
        $config = [
            "paths" => [
                "migrations" => "./src/migrations/db/migrations",
                "seeds" =>  "./src/migrations/db/seeds"
            ],
            "environments" => [
                "default_migration_table" => "phinxlog",
                "default_database" => "production",
                "production" => [
                    "adapter" => "mysql",
                    "host" => $_ENV['SQL_HOST_PROD_PIPELINE'],
                    "name" => $_ENV['SQL_DB_PROD'],
                    "user" => $_ENV['SQL_NAME_PROD'],
                    "pass" => $_ENV['SQL_PASSWORD_PROD'],
                    "port" => 3306
                ]
            ]
        ];
    }

    return $config;
}