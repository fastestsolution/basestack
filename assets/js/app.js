/**
 * zakladni inicializace JS na frontend cast CakePHP
 * 
 * pro ziskani selectoru pouzivat: document.querySelector()
 * 
 * kazdy controller je shodny s Cake controllerem a jeho akci
 * 
 */

import {Router} from './components/Router';

// Controllers
import{BaseController} from './controllers/BaseController';
import{PagesController} from './controllers/PagesController';

const router = new Router();

router.register({
	Base: BaseController,
	Pages: PagesController,
});

router.prepare();

window.onload = function (){
	router.run();
};
