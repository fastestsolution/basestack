/**
 * component for sweet alert boxs
 */

import Swal from 'sweetalert2';

class Alerts {
	constructor() {
	}

	/**
	 * alert box
	 * @param {*} message 
	 * @param {*} title 
	 */
	alert(message, title){
		var opt = {
			text: message,
			type: 'success',
			confirmButtonText: 'Pokračovat',
		};
		if (title){
			opt.title = title;
		}
		Swal.fire(opt);
	}

	/**
	 * error box
	 * @param {*} msg 
	 */
	error(msg){
		window.swal = Swal;
		window.swal.fire({
			title: 'Chyba!',
			html: msg,
			type: 'error',
			confirmButtonText: 'OK',
		});
	}
}

export default new Alerts();
