/**
 * component logging console.log if debug is enable
 */

import Enviroment from '../components/Enviroment';

class Logger {
	constructor() {
		// console.log(Enviroment);
	}

	/**
	 * zobrazeni console log
	 * @param {*} message 
	 * @param {*} color 
	 * @param {*} data 
	 */
	log(message, color, data = ''){
		if (Enviroment.debug){
			if (!color){
				color = '#666';
			}
			/* eslint-disable no-console */
			console.log('%c jsLOG: ' + message, 'color: ' + color + ';', data);
			/* eslint-enable no-console */
		}
	}

}

export default new Logger();
