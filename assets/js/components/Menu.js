/**
 * component pro adopci elementu na jine misto napr menu z paticky na top body
 * .elAdopt data-target="cilove id elementu"
 */

class ElementAdopt {
	constructor() {
	}

	init(){
		let elementToAdopt = document.querySelectorAll('.elAdopt');
		if (elementToAdopt){
			for(let element of elementToAdopt){
				let elId = element.getAttribute('data-target');
				if (document.getElementById(elId)){
					document.getElementById(elId).appendChild(element);
				}
			}
		}
	}

}

export default new ElementAdopt();
