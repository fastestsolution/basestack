/**
 * component for forms, this component can send data from forms
 */

import Alerts from '../components/Alerts';

class Forms {
	constructor() {
	}

	/**
	 * ukladani formularu formular musi mit class="SaveForm" a method="POST"
	 */
	saveForms(){
		for(let formElement of document.getElementsByClassName('SaveForm')){

			// vytvoreni spam inputu pro zablokovani robotu
			if (formElement.getElementsByClassName('.spam').length == 0){
				var input = this.newElement('input', {'type': 'hidden', 'value': 123, 'name':'spam', 'class':'spam'});
				formElement.appendChild(input);
			}

			formElement.addEventListener('submit', (event) => {
				event.preventDefault();
				let form = event.target;
				let formData = new FormData(form);

				let parsedData = [];
				for(let name of formData) {
					if (typeof(parsedData[name[0]]) == "undefined") {
						let tempdata = formData.getAll(name[0]);
						if (tempdata.length > 1) {
							parsedData.push(encodeURIComponent(name[0]) + '=' + encodeURIComponent(tempdata));
						} else {
							parsedData.push(encodeURIComponent(name[0]) + '=' + encodeURIComponent(tempdata[0]));
						}
					}
				}
				let parsedDataEncode = parsedData.join('&').replace(/%20/g, '+'); // parse object into urlEncoded String
				// console.log(parsedDataEncode);

				// nastaveni hlavicek post request
				let options = {};
				switch (form.method.toLowerCase()) {
					case 'post':
						options.body = parsedDataEncode;
						options.contentType = 'application/x-www-form-urlencoded; charset=UTF-8';
					break;
					case 'get':
						options.method = form.method;
						options.headers = {'Content-Type': 'application/json'};
					break;
				}

				// odeslani na form action
				this.postData(form.getAttribute('action'), parsedDataEncode, options).then((json)=>{
					if (json){

						// pokud je vysledek odeslani OK
						if (json.result === true){
							Alerts.alert(json.message);
						}

						// pokud je vysledek odeslani CHYBA
						if (json.result === false){
							Alerts.error(json.message);
						}

						// vymazani polozek formulare pokud je atrubut CLEAR
						if (json.clear){
							for(let element of document.getElementsByClassName("form-control")){
								element.value = '';
							}
						}
					}
				}).catch((error)=>{
					console.error('Chyba odeslani formulare', error);
				});
			});
		}
	}

	/**
	 * vytvoreni noveho elementu formulare
	 * @param {*} type 
	 * @param {*} options 
	 */
	newElement(type, options){
		let input = document.createElement(type);
		for(let key in options){
			input.setAttribute(key, options[key]);
		}
		return input;
	}

	/**
	 * odeslani dat na cakephp
	 * @param {*} url 
	 * @param {*} data 
	 */
	postData(url = ``, data = {}, options = null){
		return new Promise((resolve, reject) => {
			fetch(url, {
				method: "POST", // *GET, POST, PUT, DELETE, etc.
				mode: "cors", // no-cors, cors, *same-origin
				// 'X-CSRF-Token': Cookie.read('csrfToken'),
				cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
				credentials: "same-origin", // include, *same-origin, omit
				headers: {
					"X-Requested-With": 'XMLHttpRequest',
					"X-CSRF-Token": this.getCookie('csrfToken'),
					"Content-Type": ((options.contentType) ? options.contentType : "application/json"),
					// "Content-Type": "application/x-www-form-urlencoded",
				},
				redirect: "follow", // manual, *follow, error
				referrer: "no-referrer", // no-referrer, *client
				body: data, // body data type must match "Content-Type" header
			})
			.then(response => {
				resolve(response.json()); // parses response to JSON
			}).catch((error) => {
				reject(error);
			});
		});
	}

	/**
	 * ziskani cookie dle nazvu
	 * @param {*} cname 
	 */
	getCookie(cname) {
		var name = cname + "=";
		var ca = document.cookie.split(';');
		for(var index = 0; index < ca.length; index++) {
			var cookkie = ca[index];
			while (cookkie.charAt(0) == ' ') {
				cookkie = cookkie.substring(1);
			}
			if (cookkie.indexOf(name) == 0) {
				return cookkie.substring(name.length, cookkie.length);
			}
		}
		return "";
	}

}

export default new Forms();
