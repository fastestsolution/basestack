/**
 * component for routing JS scripts with element body attribute 
 * - data-controller
 * - data-action
 */

export class Router {
	constructor(controller = null, action = null) {
		this.controller = controller;
		this.action = action;
	}

	register(controllers = []) {
		this.controllers = controllers;
	}

	discoverRoute() {
		if (this.controller === null) {
			this.controller = document.body.getAttribute('data-controller');
		}

		if (this.action === null) {
			this.action = document.body.getAttribute('data-action');
		}
	}

	prepare() {

	}

	run() {
		this.discoverRoute();
		if (this.controller in this.controllers) {
			const controller = new this.controllers[this.controller];

			this.runController(controller);

			return controller;
		} else {
			const controller = new this.controllers['Base'];
			this.runController(controller);

			return controller;
		}
	}

	runController(controller) {
		controller.startup();
		const name = this.action.charAt(0).toUpperCase() + this.action.slice(1);
		const actionName = 'action' + name;
		const renderName = 'render' + name;
		if (actionName in controller) {
			controller[actionName]();
		}

		if ('beforeRender' in controller) {
			controller.beforeRender();
		}

		if (renderName in controller) {
			controller[renderName]();
		}
	}
}
