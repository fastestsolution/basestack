/**
 * Basic component witch use in all controllers
 */

class Enviroment {
	constructor() {
		this.env = null;
		this.debug = false;

		this.setEnviroment();
	}

	/**
	 * nastaveni typu prostredi dev/prod
	 */
	setEnviroment(){
		// pokud se jedna o localhost
		if (document.body.getAttribute('data-debug') == 1){
			this.env = 'dev';
			this.debug = true;
		} else {
			this.env = 'prod';
		}

	}
}
export default new Enviroment();
