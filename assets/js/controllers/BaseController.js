/**
 * zakladni globalni app controller
 */

import ElementAdopt from '../components/ElementAdopt';
import Menu from '../components/Menu';

// import Logger from '../components/Logger';

export class BaseController {
	startup() {
		// Logger.log('Init base controller');
	}

	beforeRender() {
		Menu.init();
		ElementAdopt.init();
		// Logger.log('render base controller');
	}
}
