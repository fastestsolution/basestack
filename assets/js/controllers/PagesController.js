import {BaseController} from './BaseController';
import Logger from '../components/Logger';
import Forms from '../components/Forms';

export class PagesController extends BaseController {
	startup() {
		Logger.log('pages startup');
	}

	actionHomepage(){
		// Logger.log('pages homepage action');
	}

	renderHomepage() {
		Logger.log('pages homepage render');

		Forms.saveForms();
	}
}
